<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/overlayDialog/dialog.php");
include("$_SERVER[DOCUMENT_ROOT]/include/phoneFormat.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include("$_SERVER[DOCUMENT_ROOT]/include/postcode/postcode.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>ATSSL: Customer Records - All Customers</title>
<link rel="stylesheet" type="text/css" href="include/style/prices.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
		
		$("#save").click(function (e) {			
			var content = $('#editable').html();	
				
			$.ajax({
				url: 'save.php',
				type: 'POST',
				data: {
                content: content
				},				
				success:function (data) {
							
					if (data == '1')
					{
						$("#status")
						.addClass("success")
						.html("Data saved successfully")
						.fadeIn('fast')
						.delay(3000)
						.fadeOut('slow');	
					}
					else
					{
						$("#status")
						.addClass("error")
						.html("An error occured, the data could not be saved")
						.fadeIn('fast')
						.delay(3000)
						.fadeOut('slow');	
					}
				}
			});   
			
		});
		
		$("#editable").click(function (e) {
			$("#save").show();
			e.stopPropagation();
		});
	
		$(document).click(function() {
			$("#save").hide();  
		});
	
	});

</script>
</head>
<body>
<?
//Select table
$result = mysql_query("SELECT * FROM monitoring_charges");

echo "<h1>System Prices (All)</h1> <br />";
echo "<div id='status'></div>";
echo "<table class='zebra'>
<thead>
<tr>
<th>Type</th>
<th>Customer Type</th>
<th>Price Ex VAT</th>
<th></th>


</tr></thead>";

while($row = mysql_fetch_array($result))
	{
		$id = $row['idmonitoring_charges'];
		echo "<tr>";
		echo "<td>" . $row['type'] . "</td>";
		echo "<td>" . $row['customer_type'] . "</td>";
		echo "<td><div id='editable' contentEditable='true'>&pound;" . $row['price_ex_vat'] . "</div></td>";
		echo "<td><button id='save'>Save</button></td>";
		echo "</tr>"; 
	
	}
include("$_SERVER[DOCUMENT_ROOT]/include/footer.php");
?>
</table>
</body>
</html>
<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>