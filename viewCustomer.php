<html>
</head>
<body>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
<link rel="stylesheet" type="text/css" href="include/style/viewCustomer.css" />
<script src="include/jquery/jquery.tools.min.js" type="text/javascript"></script>
<?php
$getid = addslashes($_GET['id']);
$formattedid = str_split($getid,6);
$customerid = $formattedid[0];
$notes = '"save.php?id=' . $customerid . '"';
?>
<script>
    $(document).ready(function() {
		
		$("#save").click(function (e) {			
			var content = $('#editable').html();	
				
			$.ajax({
				url: <?echo $notes?>,
				type: 'POST',
				data: {
                content: content
				},				
				success:function (data) {
							
					if (data == '1')
					{
						$("#status")
						.addClass("success")
						.html("Data saved successfully")
						.fadeIn('fast')
						.delay(3000)
						.fadeOut('slow');	
					}
					else
					{
						$("#status")
						.addClass("error")
						.html("An error occured, the data could not be saved")
						.fadeIn('fast')
						.delay(3000)
						.fadeOut('slow');	
					}
				}
			});   
			
		});
		
		$("#editable").click(function (e) {
			$("#save").show();
			e.stopPropagation();
		});
	
		$(document).click(function() {
			$("#save").hide();  
		});
	
	});

</script>
<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include("$_SERVER[DOCUMENT_ROOT]/include/phoneFormat.php");




$result = mysql_query("SELECT * FROM customer_details WHERE customer_id='$customerid'");
while($row = mysql_fetch_array($result))

	{
	
	if (!empty($row['customer_company_name']))
	{
		$systemName = $row['customer_company_name'];
	}else{
		$systemName = $row['customer_title'] . " " . $row['customer_first_name'] . " " . $row['customer_last_name'];
	}
	echo "<h1>Customer Details for: <u>" . $systemName . "</u><br /></h1><h3><a href=editCustomer.php?id=" . $customerid . ">[edit]</a></h3>";
?>
<div id = "wrapper">
<div id = "leftcolumn">
<h3>Personal Details</h3>
<?
$companyName = $row['customer_company_name'];


//echo "<label>Title</label>";

echo $row['customer_title'] . " ";

//echo "<label>Firstname: </label>";
echo $row['customer_first_name'] . " ";

//echo "<label>Lastname: </label>";
echo $row['customer_last_name'] . "<br />";
if($companyName!=''){
echo $companyName . "<br />";
}
echo "<h3>Invoice Address</h3>";
//echo "<label>Name/No: </label>";
echo $row['customer_address_invoice_door_number'] . " ";

//echo "<label>Street Address: </label>";
echo $row['customer_address_invoice_street_name'] . "<br />";

//echo "<label>Town: </label>";
echo $row['customer_address_invoice_town'] . "<br />";

//echo "<label>County: </label>";
echo $row['customer_address_invoice_county'] . "<br />";

//echo "<label>Country: </label>";
echo $row['customer_address_invoice_country'] . "<br />";

//echo "<label>Postcode: </label>";
echo $row['customer_address_invoice_postcode'] . "<br />";

echo "<h3>Contact Details</h3>";
echo "<label>Phone: </label>";
echo format_telfax2($row['customer_contact_phone']) . "<br />";

echo "<label>Mobile: </label>";
echo format_telfax2($row['customer_contact_mobile']) . "<br />";

echo "<label>Email: </label>";
echo $row['customer_contact_email'] . "<br /><br />";

?>
</div>

<div id = "rightcolumn">
<?

echo "<h3>Notes: </h3>";
echo "<div class='notes' id='editable' contentEditable='true'>" . $row['customer_notes'] . "</div>";
echo "<td><button id='save'>Save</button></td>";
?>



</div>
<div id="filler">



<? 
include("$_SERVER[DOCUMENT_ROOT]/include/viewCustomer/viewSystem.php");
include("$_SERVER[DOCUMENT_ROOT]/include/viewCustomer/viewJobs.php");
?>


</div>
</div>

<?
}

?>
</body>
</html>



