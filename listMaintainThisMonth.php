<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />

<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
$month = date("m-Y",strtotime("+1 months"));
//Select table
$query = "SELECT * ".
	"FROM customer_details, customer_systems, system_contacts ".
		"WHERE customer_id = customerid AND system_contract_date like '%" . $month ."' GROUP BY customer_id";
$result = mysql_query($query);

echo "<h1>Maintenances This Month (" . $month . ")</h1> <br />";
echo "<table class='zebra'>
<tr>
<th>System ID</th>
<th>Customer ID</th>
<th>Customer Name</th>
<th>System Type</th>
<th>Contracted?</th>
<th>Contract Date</th>
<th>Monitoring Type</th>
<th>Monitoring Account Number</th>
<th>Price Ex VAT</th>
<th>Site Address</th>
<th></th>
</tr>";

while($row = mysql_fetch_array($result))
	{
	$systemid = $row['system_id'];
	$address = $row['system_address_door_number']. " ". $row['system_address_street_name'] . ", " . $row['system_address_town']. ", " . $row['system_address_county']. ", " . $row['system_address_postcode'];
	  echo "<tr>";
	  echo "<td>" . $row['system_id'] . "</td>";
	  echo "<td>" . $row['customerid'] . "</td>";
	  echo "<td>" . $row['customer_title'] . " " . $row['customer_first_name'] . " ". $row['customer_last_name'] . "</td>";
	  echo "<td>" . $row['idsystem_contacts']. "</td>";
	  echo "<td>" . $row['system_contracted'] . "</td>";
	  echo "<td>" . $row['system_contract_date'] . "</td>";
	  echo "<td>" . $row['system_monitoring_type'] . "</td>";
	  echo "<td>" . $row['system_monitoring_accnt_num'] . "</td>";
	  echo "<td> &pound;" . $row['system_price_ex_vat'] . "</td>";
	  echo "<td>" . $address . "</td>";
	  echo "<td><a href=updateSystem.php?id=" . $systemid . ">edit</a></td>";
	  echo "</tr>";
	}
?>