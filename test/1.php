<!--[909x1286]-->

<div id="jpedal" style="position:relative;">

<!-- Begin shared CSS values -->
<style type="text/css" >
.t {
	position: absolute;
	-webkit-transform-origin: top left;
	-moz-transform-origin: top left;
	-o-transform-origin: top left;
	-ms-transform-origin: top left;
	-webkit-transform: scale(0.25);
	-moz-transform: scale(0.25);
	-o-transform: scale(0.25);
	-ms-transform: scale(0.25);
	z-index: 1;
	position:absolute;
	white-space:nowrap;
	overflow:visible;
}
</style>
<!-- End shared CSS values -->

<!-- Begin shared JS -->
<script type="text/javascript">

// Ensure that we're not replacing any onload events
function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			if (oldonload) {
				oldonload();
			}
			func();
		}
	}
}
addLoadEvent(function(){load1();});

function adjustWordSpacing(divName,actualWidth) {

	var el = document.getElementById(divName);
	var rawWidth=el.offsetWidth;
	if(actualWidth>rawWidth){
		var spacing= 0;
		var s=spacing+'px';
		el.style.wordSpacing = s;
		rawWidth=el.offsetWidth;

		var diff=rawWidth-actualWidth;
		if(diff<0)
			diff=-diff;
		var newDiff=diff-1;

		while (spacing<160 && (newDiff===diff || newDiff<diff)){

			diff=newDiff;
			spacing= spacing+1;

			s=spacing+'px';
			el.style.wordSpacing = s;

			newDiff=el.offsetWidth-actualWidth;
			if(newDiff<0)
				newDiff=-newDiff;
		}
		if(diff<=newDiff)
			spacing=spacing-1;
		s=spacing+'px';
		el.style.wordSpacing =s;
	}
}

</script>
<!-- End shared JS -->

<!-- Begin PHP Getters -->
<?php
	$name = $_GET["name"];
	// $name = "Boris";
?>

<!-- End PHP Getters -->

<!-- Begin inline CSS -->
<style type="text/css" >

#t1_1{left:87px;top:1222px;}
#t2_1{left:310px;top:1222px;}
#t3_1{left:723px;top:1222px;}
#t4_1{left:212px;top:115px;}
#t5_1{left:134px;top:144px;}
#t6_1{left:737px;top:144px;}
#t7_1{left:43px;top:178px;}
#t8_1{left:87px;top:213px;}
#t9_1{left:184px;top:213px;}
#ta_1{left:434px;top:213px;}
#tb_1{left:87px;top:230px;}
#tc_1{left:184px;top:230px;}
#td_1{left:434px;top:230px;}
#te_1{left:521px;top:230px;}
#tf_1{left:183px;top:247px;}
#tg_1{left:433px;top:247px;}
#th_1{left:521px;top:247px;}
#ti_1{left:87px;top:263px;}
#tj_1{left:182px;top:263px;}
#tk_1{left:431px;top:263px;}
#tl_1{left:519px;top:263px;}
#tm_1{left:87px;top:280px;}
#tn_1{left:182px;top:280px;}
#to_1{left:432px;top:280px;}
#tp_1{left:520px;top:280px;}
#tq_1{left:87px;top:297px;}
#tr_1{left:183px;top:297px;}
#ts_1{left:433px;top:297px;}
#tt_1{left:520px;top:297px;}
#tu_1{left:87px;top:314px;}
#tv_1{left:182px;top:314px;}
#tw_1{left:431px;top:314px;}
#tx_1{left:518px;top:314px;}
#ty_1{left:87px;top:331px;}
#tz_1{left:183px;top:331px;}
#t10_1{left:433px;top:331px;}
#t11_1{left:523px;top:331px;}
#t12_1{left:87px;top:347px;}
#t13_1{left:183px;top:347px;}
#t14_1{left:432px;top:347px;}
#t15_1{left:522px;top:347px;}
#t16_1{left:43px;top:382px;}
#t17_1{left:43px;top:403px;}
#t18_1{left:16px;top:439px;}
#t19_1{left:43px;top:439px;}
#t1a_1{left:60px;top:439px;}
#t1b_1{left:16px;top:455px;}
#t1c_1{left:43px;top:455px;}
#t1d_1{left:60px;top:455px;}
#t1e_1{left:70px;top:455px;}
#t1f_1{left:16px;top:472px;}
#t1g_1{left:43px;top:472px;}
#t1h_1{left:60px;top:472px;}
#t1i_1{left:16px;top:489px;}
#t1j_1{left:43px;top:489px;}
#t1k_1{left:60px;top:489px;}
#t1l_1{left:16px;top:506px;}
#t1m_1{left:43px;top:506px;}
#t1n_1{left:60px;top:506px;}
#t1o_1{left:16px;top:523px;}
#t1p_1{left:43px;top:523px;}
#t1q_1{left:60px;top:523px;}
#t1r_1{left:16px;top:539px;}
#t1s_1{left:43px;top:539px;}
#t1t_1{left:60px;top:539px;}
#t1u_1{left:341px;top:539px;}
#t1v_1{left:16px;top:556px;}
#t1w_1{left:43px;top:556px;}
#t1x_1{left:60px;top:556px;}
#t1y_1{left:401px;top:556px;}
#t1z_1{left:16px;top:573px;}
#t20_1{left:43px;top:573px;}
#t21_1{left:60px;top:573px;}
#t22_1{left:16px;top:590px;}
#t23_1{left:43px;top:590px;}
#t24_1{left:60px;top:590px;}
#t25_1{left:16px;top:607px;}
#t26_1{left:43px;top:607px;}
#t27_1{left:60px;top:607px;}
#t28_1{left:16px;top:623px;}
#t29_1{left:43px;top:623px;}
#t2a_1{left:60px;top:623px;}
#t2b_1{left:16px;top:640px;}
#t2c_1{left:43px;top:640px;}
#t2d_1{left:60px;top:640px;}
#t2e_1{left:16px;top:657px;}
#t2f_1{left:43px;top:657px;}
#t2g_1{left:60px;top:657px;}
#t2h_1{left:16px;top:674px;}
#t2i_1{left:43px;top:674px;}
#t2j_1{left:60px;top:674px;}
#t2k_1{left:16px;top:691px;}
#t2l_1{left:43px;top:691px;}
#t2m_1{left:60px;top:691px;}
#t2n_1{left:46px;top:724px;}
#t2o_1{left:43px;top:815px;}
#t2p_1{left:43px;top:845px;}
#t2q_1{left:275px;top:845px;}
#t2r_1{left:561px;top:845px;}
#t2s_1{left:848px;top:865px;}
#t2t_1{left:861px;top:865px;}
#t2u_1{left:848px;top:886px;}
#t2v_1{left:861px;top:886px;}
#t2w_1{left:848px;top:907px;}
#t2x_1{left:861px;top:907px;}
#t2y_1{left:848px;top:927px;}
#t2z_1{left:861px;top:927px;}
#t30_1{left:43px;top:1034px;}
#t31_1{left:137px;top:1034px;}
#t32_1{left:311px;top:1034px;}
#t33_1{left:691px;top:1034px;}
#t34_1{left:43px;top:1134px;}
#t35_1{left:482px;top:1134px;}

.s4_1{
	FONT-SIZE: 55px;
	FONT-FAMILY: ABCDEE-Calibri1;
	color: rgb(0,0,0);
}

.s1_1{
	FONT-SIZE: 67px;
	FONT-FAMILY: ABCDEE-Calibri1;
	color: rgb(0,0,0);
}

.s6_1{
	FONT-SIZE: 61px;
	FONT-FAMILY: ABCDEE-Calibri1;
	color: rgb(0,0,0);
}

.s3_1{
	FONT-SIZE: 67px;
	FONT-FAMILY: ABCDEE-Calibri-Bold1;
	color: rgb(0,0,0);
}

.s5_1{
	FONT-SIZE: 48px;
	FONT-FAMILY: ABCDEE-Calibri1;
	color: rgb(0,0,0);
}

.s2_1{
	FONT-SIZE: 97px;
	FONT-FAMILY: Arial, Helvetica, sans-serif;
	color: rgb(0,0,0);
	FONT-WEIGHT: bold;
}

</style>
<!-- End inline CSS -->

<!-- Begin embedded font definitions -->
<style type="text/css" >

@font-face {
	font-family: ABCDEE-Calibri1;
	src: url("1/fonts/ABCDEE-Calibri.eot");
	src: url("1/fonts/ABCDEE-Calibri.eot?#iefix") format("embedded-opentype"),
		url("1/fonts/ABCDEE-Calibri.woff") format("woff");
}

@font-face {
	font-family: ABCDEE-Calibri-Bold1;
	src: url("1/fonts/ABCDEE-Calibri-Bold.eot");
	src: url("1/fonts/ABCDEE-Calibri-Bold.eot?#iefix") format("embedded-opentype"),
		url("1/fonts/ABCDEE-Calibri-Bold.woff") format("woff");
}

</style>
<!-- End embedded font definitions -->

<!-- Begin text definitions (Positioned/styled in CSS) -->
<div id="t1_1" class="t s1_1">ISF 817 </div>
<div id="t2_1" class="t s1_1">Intruder Prevent Maintenance </div>
<div id="t3_1" class="t s1_1">Issue 4 : 05/14 </div>
<div id="t4_1" class="t s2_1">Intruder Preventative Maintenance Report</div>
<div id="t5_1" class="t s1_1">Date: <? echo date("d/m/y"); ?> </div>
<div id="t6_1" class="t s1_1">Time: </div>
<div id="t7_1" class="t s3_1">Customer Account No :- </div>
<div id="t8_1" class="t s4_1">Company/Name </div>
<div id="t9_1" class="t s4_1">: <? echo $name ?></div>
<div id="ta_1" class="t s4_1">Contact Name  : </div>
<div id="tb_1" class="t s4_1">Address </div>
<div id="tc_1" class="t s4_1">: </div>
<div id="td_1" class="t s4_1">Invoice Month </div>
<div id="te_1" class="t s4_1">: </div>
<div id="tf_1" class="t s4_1">: </div>
<div id="tg_1" class="t s4_1">CS Account No </div>
<div id="th_1" class="t s4_1">: </div>
<div id="ti_1" class="t s4_1">Town/Village </div>
<div id="tj_1" class="t s4_1">: </div>
<div id="tk_1" class="t s4_1">Install Date </div>
<div id="tl_1" class="t s4_1">: </div>
<div id="tm_1" class="t s4_1">County </div>
<div id="tn_1" class="t s4_1">: </div>
<div id="to_1" class="t s4_1">Digi/Tel No. </div>
<div id="tp_1" class="t s4_1">: </div>
<div id="tq_1" class="t s4_1">Post Code </div>
<div id="tr_1" class="t s4_1">: </div>
<div id="ts_1" class="t s4_1">Monitored  </div>
<div id="tt_1" class="t s4_1">: </div>
<div id="tu_1" class="t s4_1">Tel No. </div>
<div id="tv_1" class="t s4_1">: </div>
<div id="tw_1" class="t s4_1">Mobile No </div>
<div id="tx_1" class="t s4_1">: </div>
<div id="ty_1" class="t s4_1">CS Name </div>
<div id="tz_1" class="t s4_1">: Southern Monitoring Services Ltd </div>
<div id="t10_1" class="t s4_1">CS Tel No </div>
<div id="t11_1" class="t s4_1">: 02392 265113 </div>
<div id="t12_1" class="t s4_1">URN Intruder  </div>
<div id="t13_1" class="t s4_1">: </div>
<div id="t14_1" class="t s4_1">URN PA </div>
<div id="t15_1" class="t s4_1">: </div>
<div id="t16_1" class="t s1_1">The following checks on the intruder/hold-up alarm installation have been completed in accordance with PD 6662: 2010 </div>
<div id="t17_1" class="t s1_1">EN30131-1 Grade 1/2/3/4 </div>
<div id="t18_1" class="t s4_1">1</div>
<div id="t19_1" class="t s4_1">[  </div>
<div id="t1a_1" class="t s4_1">] Check the installation, location and siting of all equipment and devices against the system record, the as-fitted document, and log book. </div>
<div id="t1b_1" class="t s4_1">2</div>
<div id="t1c_1" class="t s4_1">[   </div>
<div id="t1d_1" class="t s4_1">] </div>
<div id="t1e_1" class="t s4_1">Check the satisfactory operation of all detection devices including hold up alarms. HUA </div>
<div id="t1f_1" class="t s4_1">3</div>
<div id="t1g_1" class="t s4_1">[ </div>
<div id="t1h_1" class="t s4_1">] Inspect all flexible connections and ensure that cable fittings and equipment are secure and adequately protected. </div>
<div id="t1i_1" class="t s4_1">4</div>
<div id="t1j_1" class="t s4_1">[ </div>
<div id="t1k_1" class="t s4_1">] Check mains, standby power and auxiliary power supplies including connections and correct charging rates </div>
<div id="t1l_1" class="t s4_1">5</div>
<div id="t1m_1" class="t s4_1">[ </div>
<div id="t1n_1" class="t s4_1">] Check the remote signalling equipment for correct operation to ARC (if applicable). </div>
<div id="t1o_1" class="t s4_1">6</div>
<div id="t1p_1" class="t s4_1">[ </div>
<div id="t1q_1" class="t s4_1">] Check line fault monitor for correct operation (if applicable). </div>
<div id="t1r_1" class="t s4_1">7</div>
<div id="t1s_1" class="t s4_1">[ </div>
<div id="t1t_1" class="t s4_1">] Check that the ARC key holder record is correct. </div>
<div id="t1u_1" class="t s4_1">If not, record correct details in “Engineers comments”. </div>
<div id="t1v_1" class="t s4_1">8</div>
<div id="t1w_1" class="t s4_1">[ </div>
<div id="t1x_1" class="t s4_1">] Check that ARC site detail (name, address etc.) are correct. </div>
<div id="t1y_1" class="t s4_1">If not, record details in “Engineers comments”. </div>
<div id="t1z_1" class="t s4_1">9</div>
<div id="t20_1" class="t s4_1">[ </div>
<div id="t21_1" class="t s4_1">] Check all audible warning and alarm devices for correct operation. </div>
<div id="t22_1" class="t s4_1">10</div>
<div id="t23_1" class="t s4_1">[ </div>
<div id="t24_1" class="t s4_1">] Check Setting &amp; unsetting. </div>
<div id="t25_1" class="t s4_1">11</div>
<div id="t26_1" class="t s4_1">[ </div>
<div id="t27_1" class="t s4_1">] Check entry Exit Procedures </div>
<div id="t28_1" class="t s4_1">12</div>
<div id="t29_1" class="t s4_1">[ </div>
<div id="t2a_1" class="t s4_1">] Check the system is fully operational. </div>
<div id="t2b_1" class="t s4_1">13</div>
<div id="t2c_1" class="t s4_1">[ </div>
<div id="t2d_1" class="t s4_1">] Check Environmental Conditions for adverse effects </div>
<div id="t2e_1" class="t s4_1">14</div>
<div id="t2f_1" class="t s4_1">[ </div>
<div id="t2g_1" class="t s4_1">] Check tamper detection on all devices </div>
<div id="t2h_1" class="t s4_1">15</div>
<div id="t2i_1" class="t s4_1">[ </div>
<div id="t2j_1" class="t s4_1">] Visual check for all potential problems (Electrical &amp; Physical) </div>
<div id="t2k_1" class="t s4_1">16</div>
<div id="t2l_1" class="t s4_1">[ </div>
<div id="t2m_1" class="t s4_1">] Preventative maintenance check completed and system left in full working order. </div>
<div id="t2n_1" class="t s4_1">The system has been left in full working order apart from the item(s) listed below </div>
<div id="t2o_1" class="t s5_1">Items not complete at the time of the check must be completed within 21 days </div>
<div id="t2p_1" class="t s3_1">Are key holders details correct? </div>
<div id="t2q_1" class="t s3_1">Name. </div>
<div id="t2r_1" class="t s3_1">Telephone No. </div>
<div id="t2s_1" class="t s3_1">[ </div>
<div id="t2t_1" class="t s3_1">] </div>
<div id="t2u_1" class="t s3_1">[ </div>
<div id="t2v_1" class="t s3_1">] </div>
<div id="t2w_1" class="t s3_1">[ </div>
<div id="t2x_1" class="t s3_1">] </div>
<div id="t2y_1" class="t s3_1">[ </div>
<div id="t2z_1" class="t s3_1">] </div>
<div id="t30_1" class="t s6_1">Completed By </div>
<div id="t31_1" class="t s6_1">:</div>
<div id="t32_1" class="t s6_1">Date :  Time Arrived : Time Left </div>
<div id="t33_1" class="t s6_1">:  </div>
<div id="t34_1" class="t s1_1">Customer/Authorised Signature:  </div>
<div id="t35_1" class="t s1_1">Print:  </div>

<!-- End text definitions -->

<!-- Begin page background -->
<div id="pg1Overlay" style="width:100%; height:100%; position:absolute; z-index:0; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
<div id="pg1"><img src="1/1.png" id="pdf1" style="width:909px; height:1286px; background-color:white;"/></div>

<!-- End page background -->

<!-- Begin page loading JS -->
<script id="ld1" type="text/javascript">

var isIE = false;
var f1 = [['t1_1',196],['t2_1',831],['t3_1',398],['t4_1',1938],['t7_1',664],['ta_1',357],['td_1',322],['tg_1',327],['tk_1',250],['to_1',263],['tq_1',224],['ts_1',251],['tu_1',157],['tw_1',233],['ty_1',199],['tz_1',772],['t10_1',210],['t11_1',346],['t12_1',309],['t14_1',172],['t16_1',3278],['t17_1',712],['t19_1',29],['t1a_1',3060],['t1c_1',41],['t1e_1',1938],['t1h_1',2584],['t1k_1',2412],['t1n_1',1880],['t1q_1',1377],['t1t_1',1097],['t1u_1',1219],['t1x_1',1340],['t1y_1',1048],['t21_1',1510],['t24_1',619],['t27_1',654],['t2a_1',859],['t2d_1',1184],['t2g_1',879],['t2j_1',1373],['t2m_1',1844],['t2n_1',1813],['t2o_1',1567],['t2p_1',880],['t2r_1',409],['t30_1',347],['t31_1',666],['t32_1',1493],['t33_1',484],['t34_1',1725],['t35_1',987]];
var c1 = 0;
function fontAdjustments1(){;
	if (c1<f1.length) {
	if (isIE) f1[c1][1] = f1[c1][1] * 4;
		adjustWordSpacing(f1[c1][0],f1[c1][1]);
		c1++;
		if (c1%10 == 0) {
			setTimeout(function(){fontAdjustments1()},0);
		} else {
			fontAdjustments1();
		}
	}
};
function load1(){
	var timeout = 100;
	if (navigator.userAgent.match(/iPhone|iPad|iPod|Android/i)) timeout = 500;
	setTimeout(fontAdjustments1,timeout);
}
</script>
<!--[if lt IE 9]><script type="text/javascript">
isIE = true;

var divCount = 114;
for (var i = 1; i < divCount; i++) {
	var div = document.getElementById("t" + i.toString(36) + "_1");
	div.style.top = (div.offsetTop * 4) + "px";
	div.style.left = (div.offsetLeft * 4) + "px";
	div.style.zoom = "25%";}
</script><![endif]-->
<!-- End page loading JS -->

</div>
