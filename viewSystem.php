<html>
</head>
<body>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
<link rel="stylesheet" type="text/css" href="include/style/viewSystem.css" />
<script src="include/jquery/jquery.tools.min.js" type="text/javascript"></script>
<?php
$getsid = addslashes($_GET['sid']);
$formattedsid = str_split($getsid,6);
$systemid = $formattedsid[0];

$notes = '"updateSystemNotes.php?id=' . $systemid . '"';
?>

<script>
    $(document).ready(function() {
		
		$("#save").click(function (e) {			
			var content = $('#editable').html();	
				
			$.ajax({
				url: <?echo $notes?>,
				type: 'POST',
				data: {
                content: content
				},				
				success:function (data) {
							
					if (data == '1')
					{
						$("#status")
						.addClass("success")
						.html("Data saved successfully")
						.fadeIn('fast')
						.delay(3000)
						.fadeOut('slow');	
					}
					else
					{
						$("#status")
						.addClass("error")
						.html("An error occured, the data could not be saved")
						.fadeIn('fast')
						.delay(3000)
						.fadeOut('slow');	
					}
				}
			});   
			
		});
		
		$("#editable").click(function (e) {
			$("#save").show();
			e.stopPropagation();
		});
	
		$(document).click(function() {
			$("#save").hide();  
		});
	
	});

</script>
<?php
include_once("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/phoneFormat.php");

$getcid = addslashes($_GET['cid']);
$formattedcid = str_split($getcid,6);
$customerid = $formattedcid[0];
$customerID = $customerid;



$system = mysql_query("SELECT * FROM customer_systems WHERE system_id='$systemid'");
while($row = mysql_fetch_array($system))
{

}


$result = mysql_query("SELECT * FROM customer_details, customer_systems WHERE customer_id='$customerid' AND customer_systems.system_id='$systemid'");
while($row = mysql_fetch_array($result))

	{
	if (!empty($row['customer_company_name']))
	{
		$systemName = "<a href='viewCustomer.php?id=" . $customerID . "'>" . $row['customer_company_name'] . "</a> (" . $row['system_type'] . ")";
	}else{
		$systemName = "<a href='viewCustomer.php?id=" . $customerID . "'>" . $row['customer_title'] . " " . $row['customer_first_name'] . " " . $row['customer_last_name'] . "</a>";
	}
	echo "<h1>System Details for: <u>" .$getsid . " - " .  $systemName . "</u><br /></h1><h3><a href=updateSystem.php?id=" . $systemid . ">[edit]</a></h3>";
?>
<div id = "wrapper">
<div id = "leftcolumn">
<?
if ($row['system_maintenance_interval']==="1"){
		$maintInt = "Yearly";
	}elseif ($row['system_maintenance_interval']==="2"){
		$maintInt = "Half-Yearly";
	}elseif ($row['system_maintenance_interval']==="3"){
		$maintInt = "Tri-Yearly";
	}elseif ($row['system_maintenance_interval']==="4"){
		$maintInt = "Quarterly";
	}elseif ($row['system_maintenance_interval']==="12"){
		$maintInt = "Monthly";
	}else{
		$maintInt = "N/A";
	}
if ($row['system_monitoring_accnt_num'] == "") {
	$monitoring = "N/A";
}else{
	$monitoring = $row['system_monitoring_accnt_num'];
}

if ($row['system_urn'] == "") {
	$urn = "N/A";
}else{
	$urn = $row['system_urn'];
}

function doDate($date)
{
	$phpdate = strtotime($date);
	$newdate = date('F', $phpdate);
	return $newdate;
} 

$contractDate = doDate($row['system_contract_date']);
echo "<h3>System Details</h3>";
echo "<label>System Type: </label>";
echo "<strong>" . $row['system_type'] . "</strong>";
echo "<br />";

echo "<label>Contracted?: </label>";
echo "<strong>" . $row['system_contracted'] . "</strong>";
echo "<br />";

echo "<label>Contract Month: </label>";
echo "<strong>" . $contractDate . "</strong>";
echo "<br />";

echo "<label>Maintenance Interval: </label>";
echo "<strong>" . $maintInt . "</strong>";
echo "<br />";

echo "<label>Monitoring Type: </label>";
echo "<strong>" . $row['system_monitoring_type'] . "</strong>";
echo "<br />";

echo "<label>Monitoring Account: </label>";
echo "<strong>" . $monitoring . "</strong>";
echo "<br />";

echo "<label>URN: </label>";
echo "<strong>" . $urn . "</strong>";
echo "<br />";

echo "<label>Maintenance Cost: </label>";
echo "<strong>" . $row['system_price_ex_vat'] . "</strong>";
echo "<br />";

echo "<label>Monitoring Cost: </label>";
echo "<strong>" . $row['system_monitoring_cost'] . "</strong>";
echo "<br />";

echo "<h3>Site Address</h3>";
$address = $row['system_address_door_number']. "<br />". $row['system_address_street_name'] . "<br />" . $row['system_address_town']. "<br />" . $row['system_address_county']. "<br />" . $row['system_address_postcode'];
echo $address;

?>



</div>
<div id="rightcolumn">
<?

echo "<h3>Notes: </h3>";
echo "<div id='editable' contentEditable='true'>" . $row['system_notes'] . "</div>";
echo "<td><button id='save'>Save</button></td>";
?>

</div>
<div id="filler">


<br />
<? 
include("$_SERVER[DOCUMENT_ROOT]/include/viewSystem/viewContacts.php");
include("$_SERVER[DOCUMENT_ROOT]/include/viewSystem/viewJobs.php");
?>


</div>
</div>

<?
}

?>
</body>
</html>



