<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
<link rel="stylesheet" type="text/css" href="include/style/print.css" />


<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
$month = addslashes($_GET['m']);
$year = addslashes($_GET['y']);
$shortMonth =  date("F",mktime(0,0,0,$month,1,0));
//Select table
$query = "SELECT * ".
	"FROM customer_details, customer_systems ".
		"WHERE customer_id = customerid AND system_contract_date < '" . $year . "-" . $month . "-31' AND system_maint_date like '%" . $month ."%' ORDER BY system_id";
$result = mysql_query($query);
$num_rows = mysql_num_rows($result);

echo "<h1>Maintenance in <u>" . $shortMonth . " " . $year . "</u> (" . $num_rows . ")</h1> <br />";

echo "<table class='zebra'>
<thead>
<tr>
<th>System ID</th>
<th>Customer ID</th>
<th>Customer Name</th>
<th>System Type</th>
<th>Contracted?</th>
<th>Contract Date</th>
<th>Monitoring Type</th>
<th>Monitoring Account Number</th>
<th>URN</th>
<th>Maintenance Cost</th>
<th>Monitoring Cost</th>
<th>Site Address</th>
<th>Contact Number</th>
<th></th>
</tr></thead>";

while($row = mysql_fetch_array($result))
	{
	$systemid = $row['system_id'];
	$address = $row['system_address_door_number']. " ". $row['system_address_street_name'] . ", " . $row['system_address_town']. ", " . $row['system_address_county']. ", " . $row['system_address_postcode'];
	  echo "<tr>";
	  echo "<td>" . $row['system_id'] . "</td>";
	  echo "<td>" . $row['customerid'] . "</td>";
	  echo "<td>" . $row['customer_title'] . " " . $row['customer_first_name'] . " ". $row['customer_last_name'] . "</td>";
	  echo "<td>" . $row['system_type']. "</td>";
	  echo "<td>" . $row['system_contracted'] . "</td>";
	  echo "<td>" . $row['system_contract_date'] . "</td>";
	  echo "<td>" . $row['system_monitoring_type'] . "</td>";
	  echo "<td>" . $row['system_monitoring_accnt_num'] . "</td>";
	  echo "<td>" . $row['system_urn'] . "</td>";
	  echo "<td> &pound;" . $row['system_price_ex_vat'] . "</td>";
	  echo "<td> &pound;" . $row['system_monitoring_cost'] . "</td>";
	  echo "<td>" . $address . "</td>";
	  echo "<td>" . $row['customer_contact_phone'] . "</td>";
	  echo "<td><a href=updateSystem.php?id=" . $systemid . ">edit</a></td>";
	  echo "</tr>";
	}
?>
</table>
</body>
</html>
<script src="include/jquery/jquery.tools.min.js"></script>