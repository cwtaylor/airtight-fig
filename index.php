<?php
//session_start();
//ob_start();
require_once "$_SERVER[DOCUMENT_ROOT]/include/autocomplete/config.php";
require_once "$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php";
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
$thisMonth = date("m",strtotime("+0 months"));
$thisYear = date("Y",strtotime("+0 months"));
?>
<!DOCTYPE html>
<html>
<head>

<title>ATSSL: Customer Records</title>
<script type="text/javascript" src="include/autocomplete/jquery.js"></script>
<script type='text/javascript' src='include/autocomplete/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="include/autocomplete/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<link rel="stylesheet" type="text/css" href="include/style/indexNew.css" />
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
</head>
<body>
<div id="wrapper">

<h2>Search</h2>
<table class="search">
<tr>
<form autocomplete="off" action="searchCustomerName.php">
<td><label>Customer Last Name: </label></td>
<td><? echo "<input type=\"text\" name=\"fName\" id=\"course\"/>";?></td>
<td><input type="submit" value="Submit" /></td>
</form>
</tr>

<tr>
<form autocomplete="off" action="searchCompanyName.php">
<td><label>Company Name: </label></td>
<td><? echo "<input type=\"text\" name=\"fName\" id=\"course\"/>";?></td>
<td><input type="submit" value="Submit" /></td>
</form>
</tr>

<tr>
<form autocomplete="on" action="searchCustomerPCode.php">
<td><label>Site Postcode: </label></td>
<td><? echo "<input type=\"text\" name=\"pCode\" id=\"course\"/>";?></td>
<td><input type="submit" value="Submit" /></td>
</form>
</tr>

<tr>
<form autocomplete="on" action="searchSystemNum.php">
<td><label>System ID: </label></td>
<td><? echo "<input type=\"text\" name=\"pCode\" id=\"course\"/>";?></td>
<td><input type="submit" value="Submit" /></td>
</form>
</tr>

<tr>
<form autocomplete="on" action="searchJobNum.php">
<td><label>Job No: </label></td>
<td><? echo "<input type=\"text\" name=\"jNum\" id=\"course\"/>";?></td>
<td><input type="submit" value="Submit" /></td>
</form>
</tr>
</table>

<div id="leftcolumn">
<h2>View</h2>
<a href="viewAllCustomer.php">Customer Details (All)</a>
<br />
<a href = "viewAllSystem.php">Customer Systems (All) </a>
<br />
<a href="systemMap.html" target="_blank">System Map</a>
<br />
<h2>Create</h2>
<a href="createCustomer.php">New Customer</a><br />
<!-- <a href="askSystemContactCustomer.php">New System Keyholder</a><br /> -->
<a href="#">Mass Import Customers</a><br />
<br />
<h2>Reports</h2>
<a href="viewMaintbyMonth.php?m=<? echo $thisMonth; ?>&y=<? echo $thisYear; ?>">Maintenance this month</a><br />
<a href="listNewSystemThisMonth.php"> New Systems this month</a><br />
<a href="listNewCustomerThisMonth.php"> New Customers this month</a><br />
<a href="viewContracted.php"> View Contracted Systems</a><br />
<a href="viewNonContracted.php"> View Non Contracted Systems</a><br /><br />
<?php include_once("$_SERVER[DOCUMENT_ROOT]/include/footer.php"); ?>
<br />
</div>
<div id="rightcolumn">
<?php include_once "$_SERVER[DOCUMENT_ROOT]/include/viewJobs.php";?>
</div>
</div>

</body>
</html>