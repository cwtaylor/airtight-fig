<!DOCTYPE html >
  <head>
  <style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0px; padding: 0px }
  #map { height: 100% }
</style>

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>ATSSL System Map</title>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&keyAIzaSyBX-eIX9FYFbob-F2Lgh8OVdvBldl54zL8"></script>
    <script type="text/javascript">
    //<![CDATA[
    google.maps.visualRefresh = true;
    var customIcons = {
      'Anti-social behaviour': {
        icon: 'images/map/alarm.png'
      },
      CCTV: {
        icon: 'images/map/cctv.png'
      },
      'Fire Alarm': {
	      icon: 'images/map/fire.png'
      },
      'Access Control': {
	      icon: 'images/map/access_control.png'
      },
      'Door Entry': {
	      icon: 'images/map/door_entry.png'
      },
      'Refuge': {
	      icon: 'images/map/refuge.png'
      },
      'Emergency Lights': {
	      icon: 'images/map/emergency_lights.png'
      },
      'Fire extinguisher': {
	      icon: 'images/map/fireexstinguisher.png'
      }
    };

    function load() {
      var map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(52.2109, 0.1985),
        zoom: 7,
        mapTypeId: 'roadmap'
      });
      var infoWindow = new google.maps.InfoWindow;

      // Change this depending on the name of your PHP file
      downloadUrl("mapGenXML.php", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          // var address = markers[i].getAttribute("address");
          var type = markers[i].getAttribute("type");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
          var html = "<b>" + type + "</b> <br/>";
          var icon = customIcons[type] || {};
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: icon.icon,
            animation: google.maps.Animation.DROP
          });
          bindInfoWindow(marker, map, infoWindow, html);
          setTimeout(function(){ marker.setAnimation(null); }, 750);
        }
      });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
        map.setCenter(marker.getPosition());
        map.setZoom(14);
      });
/*
      google.maps.event.addListener(map, 'click', function() {
	      map.setCenter(52.2109, 0.1985);
	      map.setZoom(-2);
      });
*/
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }
    function doNothing() {}

    //]]>
    

  </script>

  </head>

  <body onload="load()">
    <div id="map" style="width: 100%; height: 100%"></div>
  </body>

</html>