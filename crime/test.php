<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Google Maps JavaScript API v3 Example: Common Loader</title>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript" src="util.js"></script>
<script type="text/javascript">
  google.load("maps", "3",  {other_params:"sensor=false"});
  google.load("jquery", "1.3.2");

  function initialize() {
    var myLatlng = new google.maps.LatLng(52.2109, 0.1985);
    var myOptions = {
      zoom: 7,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    // For more information on doing XMLHR with jQuery, see these resources:
    // http://net.tutsplus.com/tutorials/javascript-ajax/use-jquery-to-retrieve-data-from-an-xml-file/
    // http://marcgrabanski.com/article/jquery-makes-parsing-xml-easy
    jQuery.get("data.xml", {}, function(data) {
      jQuery(data).find("marker").each(function() {
        var marker = jQuery(this);
        var latlng = new google.maps.LatLng(parseFloat(marker.attr("lat")),
                                    parseFloat(marker.attr("lng")));
        var marker = new google.maps.Marker({position: latlng, map: map});
     });
    });
  }

  google.setOnLoadCallback(initialize);
</script>
</head>
<body>
  <div id="map_canvas" style="width:100%; height:100%"></div>
</body>
</html>
