<?php  

include_once("connect.php");

// Start XML file, create parent node

$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node); 

// Select all the rows in the markers table

$query = "SELECT * FROM sussex WHERE crime_type = 'Burglary' LIMIT 5000";
$result = mysql_query($query);
if (!$result) {  
  die('Invalid query: ' . mysql_error());
} 

header("Content-type: text/xml"); 

// Iterate through the rows, adding XML nodes for each

while ($row = @mysql_fetch_assoc($result)){  
  // ADD TO XML DOCUMENT NODE  
  $node = $dom->createElement("marker");  
  $newnode = $parnode->appendChild($node);   
  $newnode->setAttribute("name",$row['lsoa_code']);
  // $newnode->setAttribute("address", $row['system_address_postcode']);  
  $newnode->setAttribute("lat", $row['latitude']);  
  $newnode->setAttribute("lng", $row['longitude']);  
  $newnode->setAttribute("type", $row['crime_type']);
} 

echo $dom->saveXML();

?>