<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/overlayDialog/dialog.php");
include("$_SERVER[DOCUMENT_ROOT]/include/phoneFormat.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include("$_SERVER[DOCUMENT_ROOT]/include/postcode/postcode.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>ATSSL: New Customers</title>
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
</head>
<body>
<?
//Select table
$month ="2014-" . date("m");
$result = mysql_query("SELECT * FROM customer_details WHERE customer_created LIKE '$month%'");
$numResults = mysql_num_rows($result);
if($numResults==0){
	header("location:index.php?msg=No results were found."); // Redirect to login.php page
}

echo "<h1>New Customers This Month (" . date("F") . ") (" . $numResults . ")</h1> <br />";
echo "<table class='zebra'>
<thead>
<tr>
<th>ID</th>
<th>Title</th>
<th>First name</th>
<th>Last name</th>
<th>Company Name</th>
<th>Invoice Address</th>
<th>Phone</th>
<th>Mobile</th>
<th>Email</th>
<th></th>
<th></th>


</tr></thead>";

while($row = mysql_fetch_array($result))
	{
	$customerid = $row['customer_id'];
	if ($row['customer_address_invoice_door_number']==""){
		$invoice = "";
	}else{
		$invoice = $row['customer_address_invoice_door_number']. " ". $row['customer_address_invoice_street_name'] . ", " . $row['customer_address_invoice_town']. ", " . $row['customer_address_invoice_county']. ", " . strtoupper($row['customer_address_invoice_postcode']);
	}
	if ($row['customer_address_door_number']==""){
		$address = "";
	}else{
		$address = $row['customer_address_door_number']. " ". $row['customer_address_street_name'] . ", " . $row['customer_address_town']. ", " . $row['customer_address_county']. ", " . $row['customer_address_postcode'];
	}
		echo "<tr>";
		echo "<td>" . $row['customer_id'] . "</td>";
		echo "<td>" . $row['customer_title'] . "</td>";
		echo "<td>" . $row['customer_first_name'] . "</td>";
		echo "<td>" . $row['customer_last_name'] . "</td>";
		echo "<td>" . $row['customer_company_name'] . "</td>";
		echo "<td>" . $invoice . "</td>";
		echo "<td>" . format_telfax2($row['customer_contact_phone']) . "</td>";
		echo "<td>" . format_telfax2($row['customer_contact_mobile']) . "</td>";
		echo "<td>" . $row['customer_contact_email'] . "</td>";
		echo "<td><a href=viewCustomer.php?id=" . $customerid . ">view</a>/<a href=editCustomer.php?id=" . $customerid . ">edit</a>/<a href=createJob.php?cid=" . $customerid . ">job</a></td>";
		echo "<td><button class='modalInput' rel='#yesno'>Delete</button></td>";
		echo "</tr>"; 
	
	}
include("$_SERVER[DOCUMENT_ROOT]/include/footer.php");
?>
</table>
</body>
</html>
<script src="include/jquery/jquery.tools.min.js"></script>