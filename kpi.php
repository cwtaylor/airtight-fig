<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/getStats.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/getField.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/cron/maintJobStats.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/systemValue.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/snips.php");

clearTable();
doStuff();
?>
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />

<?

function formatResponse($value)
{
	if($value<1){
			$response_time = "<font color='green'>" . $value * 60 . " minutes</font>";
		}elseif($value<24){
			$response_time = "<font color='orange'>" . $value . " hours</font>";
		}else{
			$response_time = "<font color='red'>" . round($value /24,0) . " days</font>";
		}
		
	return $response_time;
}

function formatPercent($value)
{
	if($value<50){
			$percent = "<font color='red'>" . $value . "%</font>";
		}elseif($value<80){
			$percent = "<font color='orange'>" . $value . "%</font>";
		}else{
			$percent = "<font color='green'>" . $value . "%</font>";
		}
		
	return $percent;
}

function percent($num_amount, $num_total) {
	$count1 = $num_amount / $num_total;
	$count2 = $count1 * 100;
	$count = number_format($count2, 0);
	return $count;
}

$jan14New = getNewSystemTotal("2014-01", "yes");
$feb14New = getNewSystemTotal("2014-02", "yes");
$mar14New = getNewSystemTotal("2014-03", "yes");
$apr14New = getNewSystemTotal("2014-04", "yes");
$may14New = getNewSystemTotal("2014-05", "yes");
$jun14New = getNewSystemTotal("2014-06", "yes");
$jul14New = getNewSystemTotal("2014-07", "yes");
$aug14New = getNewSystemTotal("2014-08", "yes");
$sept14New = getNewSystemTotal("2014-09", "yes");
$oct14New = getNewSystemTotal("2014-10", "yes");
$nov14New = getNewSystemTotal("2014-11", "yes");
$dec14New = getNewSystemTotal("2014-12", "yes");

$jan14NewNon = getNewSystemTotal("2014-01", "no");
$feb14NewNon = getNewSystemTotal("2014-02", "no");
$mar14NewNon = getNewSystemTotal("2014-03", "no");
$apr14NewNon = getNewSystemTotal("2014-04", "no");
$may14NewNon = getNewSystemTotal("2014-05", "no");
$jun14NewNon = getNewSystemTotal("2014-06", "no");
$jul14NewNon = getNewSystemTotal("2014-07", "no");
$aug14NewNon = getNewSystemTotal("2014-08", "no");
$sept14NewNon = getNewSystemTotal("2014-09", "no");
$oct14NewNon = getNewSystemTotal("2014-10", "no");
$nov14NewNon = getNewSystemTotal("2014-11", "no");
$dec14NewNon = getNewSystemTotal("2014-12", "no");

$jan14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "01");
$feb14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "02");
$mar14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "03");
$apr14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "04");
$may14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "05");
$jun14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "06");
$jul14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "07");
$aug14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "08");
$sept14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "09");
$oct14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "10");
$nov14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "11");
$dec14Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2014", "12");


$jan13New = getNewSystemTotal("2013-01", "yes");
$feb13New = getNewSystemTotal("2013-02", "yes");
$mar13New = getNewSystemTotal("2013-03", "yes");
$apr13New = getNewSystemTotal("2013-04", "yes");
$may13New = getNewSystemTotal("2013-05", "yes");
$jun13New = getNewSystemTotal("2013-06", "yes");
$jul13New = getNewSystemTotal("2013-07", "yes");
$aug13New = getNewSystemTotal("2013-08", "yes");
$sept13New = getNewSystemTotal("2013-09", "yes");
$oct13New = getNewSystemTotal("2013-10", "yes");
$nov13New = getNewSystemTotal("2013-11", "yes");
$dec13New = getNewSystemTotal("2013-12", "yes");

$jan13NewNon = getNewSystemTotal("2013-01", "no");
$feb13NewNon = getNewSystemTotal("2013-02", "no");
$mar13NewNon = getNewSystemTotal("2013-03", "no");
$apr13NewNon = getNewSystemTotal("2013-04", "no");
$may13NewNon = getNewSystemTotal("2013-05", "no");
$jun13NewNon = getNewSystemTotal("2013-06", "no");
$jul13NewNon = getNewSystemTotal("2013-07", "no");
$aug13NewNon = getNewSystemTotal("2013-08", "no");
$sept13NewNon = getNewSystemTotal("2013-09", "no");
$oct13NewNon = getNewSystemTotal("2013-10", "no");
$nov13NewNon = getNewSystemTotal("2013-11", "no");
$dec13NewNon = getNewSystemTotal("2013-12", "no");


//Total Maint Per Month
$jan13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "01");
$feb13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "02");
$mar13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "03");
$apr13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "04");
$may13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "05");
$jun13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "06");
$jul13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "07");
$aug13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "08");
$sept13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "09");
$oct13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "10");
$nov13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "11");
$dec13Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2013", "12");

//Maint Progress Per Month
$jan13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'01'", "progress"), $jan13Maint));
$feb13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'02'", "progress"), $feb13Maint));
$mar13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'03'", "progress"), $mar13Maint));
$apr13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'04'", "progress"), $apr13Maint));
$may13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'05'", "progress"), $may13Maint));
$jun13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'06'", "progress"), $jun13Maint));
$jul13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'07'", "progress"), $jul13Maint));
$aug13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'08'", "progress"), $aug13Maint));
$sept13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'09'", "progress"), $sept13Maint));
$oct13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'10'", "progress"), $oct13Maint));
$nov13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'11'", "progress"), $nov13Maint));
$dec13MaintProg = formatPercent(percent(getField("maint_progress_13", "month", "'12'", "progress"), $dec13Maint));

$jan12New = getNewSystemTotal("2012-01", "yes");
$feb12New = getNewSystemTotal("2012-02", "yes");
$mar12New = getNewSystemTotal("2012-03", "yes");
$apr12New = getNewSystemTotal("2012-04", "yes");
$may12New = getNewSystemTotal("2012-05", "yes");
$jun12New = getNewSystemTotal("2012-06", "yes");
$jul12New = getNewSystemTotal("2012-07", "yes");
$aug12New = getNewSystemTotal("2012-08", "yes");
$sept12New = getNewSystemTotal("2012-09", "yes");
$oct12New = getNewSystemTotal("2012-10", "yes");
$nov12New = getNewSystemTotal("2012-11", "yes");
$dec12New = getNewSystemTotal("2012-12", "yes");

$jan12NewNon = getNewSystemTotal("2012-01", "no");
$feb12NewNon = getNewSystemTotal("2012-02", "no");
$mar12NewNon = getNewSystemTotal("2012-03", "no");
$apr12NewNon = getNewSystemTotal("2012-04", "no");
$may12NewNon = getNewSystemTotal("2012-05", "no");
$jun12NewNon = getNewSystemTotal("2012-06", "no");
$jul12NewNon = getNewSystemTotal("2012-07", "no");
$aug12NewNon = getNewSystemTotal("2012-08", "no");
$sept12NewNon = getNewSystemTotal("2012-09", "no");
$oct12NewNon = getNewSystemTotal("2012-10", "no");
$nov12NewNon = getNewSystemTotal("2012-11", "no");
$dec12NewNon = getNewSystemTotal("2012-12", "no");


//Total Maint Per Month
$jan12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "01");
$feb12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "02");
$mar12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "03");
$apr12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "04");
$may12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "05");
$jun12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "06");
$jul12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "07");
$aug12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "08");
$sept12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "09");
$oct12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "10");
$nov12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "11");
$dec12Maint = getCountTotal2("customer_systems", "system_contract_date", "system_maint_date", "2012", "12");

//Maint Progress Per Month
$jan12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'01'", "progress"), $jan12Maint));
$feb12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'02'", "progress"), $feb12Maint));
$mar12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'03'", "progress"), $mar12Maint));
$apr12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'04'", "progress"), $apr12Maint));
$may12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'05'", "progress"), $may12Maint));
$jun12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'06'", "progress"), $jun12Maint));
$jul12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'07'", "progress"), $jul12Maint));
$aug12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'08'", "progress"), $aug12Maint));
$sept12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'09'", "progress"), $sept12Maint));
$oct12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'10'", "progress"), $oct12Maint));
$nov12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'11'", "progress"), $nov12Maint));
$dec12MaintProg = formatPercent(percent(getField("maint_progress_12", "month", "'12'", "progress"), $dec12Maint));

// //Contract Value per Month
$jan12Value = getTotal("01", "12");
$feb12Value = getTotal("02", "12");
$mar12Value = getTotal("03", "12");
$apr12Value = getTotal("04", "12");
$may12Value = getTotal("05", "12");
$jun12Value = getTotal("06", "12");
$jul12Value = getTotal("07", "12");
$aug12Value = getTotal("08", "12");
$sept12Value = getTotal("09", "12");
$oct12Value = getTotal("10", "12");
$nov12Value = getTotal("11", "12");
$dec12Value = getTotal("12", "12");
$total12 = getTotal("all", "12");

// //Contract Value per Month
$jan13Value = getTotal("01", "13");
$feb13Value = getTotal("02", "13");
$mar13Value = getTotal("03", "13");
$apr13Value = getTotal("04", "13");
$may13Value = getTotal("05", "13");
$jun13Value = getTotal("06", "13");
$jul13Value = getTotal("07", "13");
$aug13Value = getTotal("08", "13");
$sept13Value = getTotal("09", "13");
$oct13Value = getTotal("10", "13");
$nov13Value = getTotal("11", "13");
$dec13Value = getTotal("12", "13");
$total13 = getTotal("all", "13");

// //Contract Value per Month
$jan14Value = getTotal("01", "14");
$feb14Value = getTotal("02", "14");
$mar14Value = getTotal("03", "14");
$apr14Value = getTotal("04", "14");
$may14Value = getTotal("05", "14");
$jun14Value = getTotal("06", "14");
$jul14Value = getTotal("07", "14");
$aug14Value = getTotal("08", "14");
$sept14Value = getTotal("09", "14");
$oct14Value = getTotal("10", "14");
$nov14Value = getTotal("11", "14");
$dec14Value = getTotal("12", "14");
$total14 = getTotal("all", "14");

// $total14 = $jan14Value + $feb14Value + $mar14Value + $apr14Value + $may14Value + $jun14Value + $jul14Value + $aug14Value + $sept14Value + $oct14Value + $nov14Value + $dec14Value ;

/* echo getField("fig_users","username","'cwt'","email"); */
//Select table

$result = mysql_query("SELECT AVG(job_response_time)  
FROM service_jobs_core");  

while($row=mysql_fetch_array($result))  
{  
	$a_response_time = formatResponse($row['AVG(job_response_time)']);
}
$result = mysql_query("SELECT *, MAX(job_response_time)
FROM service_jobs_core");
while($row=mysql_fetch_array($result))  
{  
	$l_response_time = formatResponse($row['MAX(job_response_time)']);
}
$result = mysql_query("SELECT MIN(job_response_time)
FROM service_jobs_core");
while($row=mysql_fetch_array($result))  
{  
	$s_response_time = formatResponse($row['MIN(job_response_time)']);
}
echo "<h1>Key Performance Indicators (All)</h1> <br />";
echo "<div id='status'></div>";
echo "<h2>Job KPIs</h2>";
echo "<table class='zebra'>
<thead>
<tr>
<th>KPI</th>
<th>Value</th>
</tr></thead>";

echo "<tr>";
	echo "<td>Average Response Time</td>";
	echo "<td>" . $a_response_time . "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td>Longest Response Time</td>";
	echo "<td>" . $l_response_time . "</td>";
echo "</tr>";
echo "<tr>";
	echo "<td>Shortest Response Time</td>";
	echo "<td>" . $s_response_time . "</td>";
echo "</tr>";
echo "</table>";
?>
<br />
<h2>Performance 2014</h2>
<table class="zebra">
<thead>
<tr>
<th>KPI/Month</th>
<th>January</th>
<th>Febuary</th>
<th>March</th>
<th>April</th>
<th>May</th>
<th>June</th>
<th>July</th>
<th>August</th>
<th>September</th>
<th>October</th>
<th>November</th>
<th>December</th>
<th>Total</th>
</tr></thead>

<tr>
<td><strong>New Systems (Contract)</strong></td>
<td><? echo $jan14New; ?></td>
<td><? echo $feb14New; ?></td>
<td><? echo $mar14New; ?></td>
<td><? echo $apr14New; ?></td>
<td><? echo $may14New; ?></td>
<td><? echo $jun14New; ?></td>
<td><? echo $jul14New; ?></td>
<td><? echo $aug14New; ?></td>
<td><? echo $sept14New; ?></td>
<td><? echo $oct14New; ?></td>
<td><? echo $nov14New; ?></td>
<td><? echo $dec14New; ?></td>
<td><? echo $jan14New + $feb14New + $mar14New + $apr14New + $may14New + $jun14New + $jul14New + $aug14New + $sept14New + $oct14New + $nov14New + $dec14New ?></td>
</tr>

<tr>
<td><strong>New Systems (Non-Contract)</strong></td>
<td><? echo $jan14NewNon; ?></td>
<td><? echo $feb14NewNon; ?></td>
<td><? echo $mar14NewNon; ?></td>
<td><? echo $apr14NewNon; ?></td>
<td><? echo $may14NewNon; ?></td>
<td><? echo $jun14NewNon; ?></td>
<td><? echo $jul14NewNon; ?></td>
<td><? echo $aug14NewNon; ?></td>
<td><? echo $sept14NewNon; ?></td>
<td><? echo $oct14NewNon; ?></td>
<td><? echo $nov14NewNon; ?></td>
<td><? echo $dec14NewNon; ?></td>
<td><? echo $jan14NewNon + $feb14NewNon + $mar14NewNon + $apr14NewNon + $may14NewNon + $jun14NewNon + $jul14NewNon + $aug14NewNon + $sept14NewNon + $oct14NewNon + $nov14NewNon + $dec14NewNon ?></td>
</tr>

<tr>
<td><strong>Lost Contracts</strong></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><strong>Total Maintenance Per Month</strong></td>
<td><a href='viewMaintbyMonth.php?m=01&y=2014'><? echo $jan14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=02&y=2014'><? echo $feb14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=03&y=2014'><? echo $mar14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=04&y=2014'><? echo $apr14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=05&y=2014'><? echo $may14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=06&y=2014'><? echo $jun14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=07&y=2014'><? echo $jul14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=08&y=2014'><? echo $aug14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=09&y=2014'><? echo $sept14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=10&y=2014'><? echo $oct14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=11&y=2014'><? echo $nov14Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=12&y=2014'><? echo $dec14Maint; ?></a></td>
<td><? echo $jan14Maint + $feb14Maint + $mar14Maint + $apr14Maint + $may14Maint + $jun14Maint + $jul14Maint + $aug14Maint + $sept14Maint + $oct14Maint + $nov14Maint + $dec14Maint; ?></td>
</tr>

<tr>
<td><strong>Maintenance Hit Rate</strong></td>
<td><? echo $jan14MaintProg; ?></td>
<td><? echo $feb14MaintProg; ?></td>
<td><? echo $mar14MaintProg; ?></td>
<td><? echo $apr14MaintProg; ?></td>
<td><? echo $may14MaintProg; ?></td>
<td><? echo $jun14MaintProg; ?></td>
<td><? echo $jul14MaintProg; ?></td>
<td><? echo $aug14MaintProg; ?></td>
<td><? echo $sept14MaintProg; ?></td>
<td><? echo $oct14MaintProg; ?></td>
<td><? echo $nov14MaintProg; ?></td>
<td><? echo $dec14MaintProg; ?></td>
<td></td>
</tr>

<tr>
<td><strong>Contract Value</strong></td>
<td><? echo $jan14Value; ?></td>
<td><? echo $feb14Value; ?></td>
<td><? echo $mar14Value; ?></td>
<td><? echo $apr14Value; ?></td>
<td><? echo $may14Value; ?></td>
<td><? echo $jun14Value; ?></td>
<td><? echo $jul14Value; ?></td>
<td><? echo $aug14Value; ?></td>
<td><? echo $sept14Value; ?></td>
<td><? echo $oct14Value; ?></td>
<td><? echo $nov14Value; ?></td>
<td><? echo $dec14Value; ?></td>
<td><? echo $total14; ?></td>
</tr>



</table>
<h2>Performance 2013</h2>
<table class="zebra">
<thead>
<tr>
<th>KPI/Month</th>
<th>January</th>
<th>Febuary</th>
<th>March</th>
<th>April</th>
<th>May</th>
<th>June</th>
<th>July</th>
<th>August</th>
<th>September</th>
<th>October</th>
<th>November</th>
<th>December</th>
<th>Total</th>
</tr></thead>

<tr>
<td><strong>New Systems (Contract)</strong></td>
<td><? echo $jan13New; ?></td>
<td><? echo $feb13New; ?></td>
<td><? echo $mar13New; ?></td>
<td><? echo $apr13New; ?></td>
<td><? echo $may13New; ?></td>
<td><? echo $jun13New; ?></td>
<td><? echo $jul13New; ?></td>
<td><? echo $aug13New; ?></td>
<td><? echo $sept13New; ?></td>
<td><? echo $oct13New; ?></td>
<td><? echo $nov13New; ?></td>
<td><? echo $dec13New; ?></td>
<td><? echo $jan13New + $feb13New + $mar13New + $apr13New + $may13New + $jun13New + $jul13New + $aug13New + $sept13New + $oct13New + $nov13New + $dec13New ?></td>
</tr>

<tr>
<td><strong>New Systems (Non-Contract)</strong></td>
<td><? echo $jan13NewNon; ?></td>
<td><? echo $feb13NewNon; ?></td>
<td><? echo $mar13NewNon; ?></td>
<td><? echo $apr13NewNon; ?></td>
<td><? echo $may13NewNon; ?></td>
<td><? echo $jun13NewNon; ?></td>
<td><? echo $jul13NewNon; ?></td>
<td><? echo $aug13NewNon; ?></td>
<td><? echo $sept13NewNon; ?></td>
<td><? echo $oct13NewNon; ?></td>
<td><? echo $nov13NewNon; ?></td>
<td><? echo $dec13NewNon; ?></td>
<td><? echo $jan13NewNon + $feb13NewNon + $mar13NewNon + $apr13NewNon + $may13NewNon + $jun13NewNon + $jul13NewNon + $aug13NewNon + $sept13NewNon + $oct13NewNon + $nov13NewNon + $dec13NewNon ?></td>
</tr>

<tr>
<td><strong>Lost Contracts</strong></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><strong>Total Maintenance Per Month</strong></td>
<td><a href='viewMaintbyMonth.php?m=01&y=2013'><? echo $jan13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=02&y=2013'><? echo $feb13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=03&y=2013'><? echo $mar13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=04&y=2013'><? echo $apr13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=05&y=2013'><? echo $may13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=06&y=2013'><? echo $jun13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=07&y=2013'><? echo $jul13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=08&y=2013'><? echo $aug13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=09&y=2013'><? echo $sept13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=10&y=2013'><? echo $oct13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=11&y=2013'><? echo $nov13Maint; ?></a></td>
<td><a href='viewMaintbyMonth.php?m=12&y=2013'><? echo $dec13Maint; ?></a></td>
<td><? echo $jan13Maint + $feb13Maint + $mar13Maint + $apr13Maint + $may13Maint + $jun13Maint + $jul13Maint + $aug13Maint + $sept13Maint + $oct13Maint + $nov13Maint + $dec13Maint; ?></td>
</tr>

<tr>
<td><strong>Maintenance Hit Rate</strong></td>
<td><? echo $jan13MaintProg; ?></td>
<td><? echo $feb13MaintProg; ?></td>
<td><? echo $mar13MaintProg; ?></td>
<td><? echo $apr13MaintProg; ?></td>
<td><? echo $may13MaintProg; ?></td>
<td><? echo $jun13MaintProg; ?></td>
<td><? echo $jul13MaintProg; ?></td>
<td><? echo $aug13MaintProg; ?></td>
<td><? echo $sept13MaintProg; ?></td>
<td><? echo $oct13MaintProg; ?></td>
<td><? echo $nov13MaintProg; ?></td>
<td><? echo $dec13MaintProg; ?></td>
<td></td>
</tr>

<tr>
<td><strong>Contract Value</strong></td>
<td><? echo $jan13Value; ?></td>
<td><? echo $feb13Value; ?></td>
<td><? echo $mar13Value; ?></td>
<td><? echo $apr13Value; ?></td>
<td><? echo $may13Value; ?></td>
<td><? echo $jun13Value; ?></td>
<td><? echo $jul13Value; ?></td>
<td><? echo $aug13Value; ?></td>
<td><? echo $sept13Value; ?></td>
<td><? echo $oct13Value; ?></td>
<td><? echo $nov13Value; ?></td>
<td><? echo $dec13Value; ?></td>
<td><? echo $total13; ?></td>
</tr>



</table>

<h2>Performance 2012</h2>
<table class="zebra">
<thead>
<tr>
<th>KPI/Month</th>
<th>January</th>
<th>Febuary</th>
<th>March</th>
<th>April</th>
<th>May</th>
<th>June</th>
<th>July</th>
<th>August</th>
<th>September</th>
<th>October</th>
<th>November</th>
<th>December</th>
<th>Total</th>
</tr></thead>

<tr>
<td><strong>New Systems (Contract)</strong></td>
<td><? echo $jan12New; ?></td>
<td><? echo $feb12New; ?></td>
<td><? echo $mar12New; ?></td>
<td><? echo $apr12New; ?></td>
<td><? echo $may12New; ?></td>
<td><? echo $jun12New; ?></td>
<td><? echo $jul12New; ?></td>
<td><? echo $aug12New; ?></td>
<td><? echo $sept12New; ?></td>
<td><? echo $oct12New; ?></td>
<td><? echo $nov12New; ?></td>
<td><? echo $dec12New; ?></td>
<td><? echo $jan12New + $feb12New + $mar12New + $apr12New + $may12New + $jun12New + $jul12New + $aug12New + $sept12New + $oct12New + $nov12New + $dec12New ?></td>
</tr>

<tr>
<td><strong>New Systems (Non-Contract)</strong></td>
<td><? echo $jan12NewNon; ?></td>
<td><? echo $feb12NewNon; ?></td>
<td><? echo $mar12NewNon; ?></td>
<td><? echo $apr12NewNon; ?></td>
<td><? echo $may12NewNon; ?></td>
<td><? echo $jun12NewNon; ?></td>
<td><? echo $jul12NewNon; ?></td>
<td><? echo $aug12NewNon; ?></td>
<td><? echo $sept12NewNon; ?></td>
<td><? echo $oct12NewNon; ?></td>
<td><? echo $nov12NewNon; ?></td>
<td><? echo $dec12NewNon; ?></td>
<td><? echo $jan12NewNon + $feb12NewNon + $mar12NewNon + $apr12NewNon + $may12NewNon + $jun12NewNon + $jul12NewNon + $aug12NewNon + $sept12NewNon + $oct12NewNon + $nov12NewNon + $dec12NewNon ?></td>
</tr>

<tr>
<td><strong>Lost Contracts</strong></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><strong>Total Maintenance Per Month</strong></td>
<td><? echo $jan12Maint; ?></td>
<td><? echo $feb12Maint; ?></td>
<td><? echo $mar12Maint; ?></td>
<td><? echo $apr12Maint; ?></td>
<td><? echo $may12Maint; ?></td>
<td><? echo $jun12Maint; ?></td>
<td><? echo $jul12Maint; ?></td>
<td><? echo $aug12Maint; ?></td>
<td><? echo $sept12Maint; ?></td>
<td><? echo $oct12Maint; ?></td>
<td><? echo $nov12Maint; ?></td>
<td><? echo $dec12Maint; ?></td>
<td></td>
</tr>

<tr>
<td><strong>Maintenance Hit Rate</strong></td>
<td><? echo $jan12MaintProg; ?></td>
<td><? echo $feb12MaintProg; ?></td>
<td><? echo $mar12MaintProg; ?></td>
<td><? echo $apr12MaintProg; ?></td>
<td><? echo $may12MaintProg; ?></td>
<td><? echo $jun12MaintProg; ?></td>
<td><? echo $jul12MaintProg; ?></td>
<td><? echo $aug12MaintProg; ?></td>
<td><? echo $sept12MaintProg; ?></td>
<td><? echo $oct12MaintProg; ?></td>
<td><? echo $nov12MaintProg; ?></td>
<td><? echo $dec12MaintProg; ?></td>
<td></td>
</tr>

<tr>
<td><strong>Contract Value</strong></td>
<td><? echo $jan12Value; ?></td>
<td><? echo $feb12Value; ?></td>
<td><? echo $mar12Value; ?></td>
<td><? echo $apr12Value; ?></td>
<td><? echo $may12Value; ?></td>
<td><? echo $jun12Value; ?></td>
<td><? echo $jul12Value; ?></td>
<td><? echo $aug12Value; ?></td>
<td><? echo $sept12Value; ?></td>
<td><? echo $oct12Value; ?></td>
<td><? echo $nov12Value; ?></td>
<td><? echo $dec12Value; ?></td>
<td><? echo $total12; ?></td>
</tr>

</table>

