<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>ATSSL: Customer Records - All Systems</title>
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
</head>
<body>
<?
//Select table
$postCode = $_GET['pCode'];
if (empty($postCode)) {
	header("location:index.php?msg=No search term entered."); // Redirect to login.php page
}
$query = "SELECT * ".
	"FROM customer_details, customer_systems ".
		"WHERE system_address_postcode LIKE '$postCode%' AND customer_id = customerid";
$result = mysql_query($query);
$numResults = mysql_num_rows($result);
if($numResults==0){
	header("location:index.php?msg=No results were found."); // Redirect to login.php page
}


echo "<h1>Customers Systems (" . $numResults . " Found)</h1> <br />";
echo "<table class='zebra'>
<tr>
<th>System ID</th>
<th>Customer ID</th>
<th>Customer Name</th>
<th>System Type</th>
<th>Contracted?</th>
<th>Contract Date</th>
<th>Maintenance Interval</th>
<th>Monitoring Type</th>
<th>Monitoring Account Number</th>
<th>Price Ex VAT</th>
<th>Site Address</th>
<th></th>
</tr>";

while($row = mysql_fetch_array($result))
	{
	if ($row['system_maintenance_interval']==="1"){
		$maintInt = "Yearly";
	}elseif ($row['system_maintenance_interval']==="2"){
		$maintInt = "Half-Yearly";
	}elseif ($row['system_maintenance_interval']==="3"){
		$maintInt = "Tri-Yearly";
	}elseif ($row['system_maintenance_interval']==="4"){
		$maintInt = "Quarterly";
	}else{
		$maintInt = "N/A";
	}
	$systemid = $row['system_id'];
	$address = $row['system_address_door_number']. " ". $row['system_address_street_name'] . ", " . $row['system_address_town']. ", " . $row['system_address_county']. ", " . $row['system_address_postcode'];
	  echo "<tr>";
	  echo "<td>" . $row['system_id'] . "</td>";
	  echo "<td>" . $row['customerid'] . "</td>";
	  echo "<td>" . $row['customer_title'] . " " . $row['customer_first_name'] . " ". $row['customer_last_name'] . "</td>";
	  echo "<td>" . $row['system_type']. "</td>";
	  echo "<td>" . $row['system_contracted'] . "</td>";
	  echo "<td>" . $row['system_contract_date'] . "</td>";
	  echo "<td>" . $maintInt . "</td>";
	  echo "<td>" . $row['system_monitoring_type'] . "</td>";
	  echo "<td>" . $row['system_monitoring_accnt_num'] . "</td>";
	  echo "<td> &pound;" . $row['system_price_ex_vat'] . "</td>";
	  echo "<td>" . $address . "</td>";
	  echo "<td><a href=viewSystem.php?cid=" .$row['customer_id'] . "&sid=" . $row['system_id'] . ">view</a> / <a href=updateSystem.php?id=" . $systemid . ">edit</a></td>";
	  echo "</tr>";
	  $maintInt = "";
	}
include("$_SERVER[DOCUMENT_ROOT]/include/footer.php");
?>