<?php
//log them out
$logout = $_GET['logout'];
if ($logout == "yes") { //destroy the session
    session_start();
    $_SESSION = array();
    session_destroy();
}

//force the browser to use ssl (STRONGLY RECOMMENDED!!!!!!!!)
if ($_SERVER["SERVER_PORT"] != 443){ 
    header("Location: https://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']); 
    exit(); 
}

//you should look into using PECL filter or some form of filtering here for POST variables
$username = strtoupper($_POST["username"]); //remove case sensitivity on the username
$password = $_POST["password"];
$formage = $_POST["formage"];

if ($_POST["oldform"]) { //prevent null bind

    if ($username != NULL && $password != NULL){
        //include the class and create a connection
        // include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
        include ("$_SERVER[DOCUMENT_ROOT]/adLDAP/src/adLDAP.php");
        try {
            $adldap = new adLDAP();
        }
        catch (adLDAPException $e) {
            echo $e; 
            exit();   
        }
        
        //authenticate the user
        if ($adldap->authenticate($username, $password)){
            //establish your session and redirect
            // session_start();
            session_register("password");
            session_register("admin");
            $userinfo = $adldap->user()->info($username, array("samAccountName","givenname","sn","displayname","objectguid","memberof"));
            $_SESSION["username"] = $username;
            $_SESSION["allInfo"] = $userinfo;
            $_SESSION["firstname"] = $userinfo[0]["givenname"][0];
            $_SESSION["lastname"] = $userinfo[0]["sn"][0];
            $_SESSION["displayname"] = $userinfo[0]["displayname"][0];
            $_SESSION['expiry'] = $_SESSION['start'] + (120 * 60);

            
            if ($adldap->user()->inGroup($username, "FIG Admins")=="1"){
                $_SESSION["admin"] = 'true';
            }else{
                $_SESSION["admin"] = 'false';
            }
            

            // echo $userinfo[0]["displayname"][0];
            $redir = "Location:index.php";
            header($redir);
            exit;
        }
    }
    $failed = 1;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ATSSL: Customer Records</title>
<link rel="stylesheet" type="text/css" href="include/style/login2.css"/>
</head>

<body>
<form name="login" id="login" method='post' action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
<input type='hidden' name='oldform' value='1'>
<h2><img width="380" height="120" src="images/Logo_Main.jpg" alt="logo"></h2>
    <h1>Log In</h1>
	<p>Please note: <strong>Unauthorized use of this site is prohibited and may be subject to civil and criminal prosecution.</strong></p>
	<?$msg = $_GET['msg'];  //GET the message
	if($msg!='') echo '<p><span style="color:green">'.$msg.'</span></p>';
	?>
    <fieldset id="inputs">
        <input name="username" id="username" type="text" placeholder="Username" autofocus required>   
        <input name="password" id="password" type="password" placeholder="Password" required>
    </fieldset>
    <fieldset id="actions">
        <input type="submit" id="submit" value="Submit">
        <?php if ($failed){ echo ("<br>Login Failed!<br><br>\n"); } ?>
        <?php if ($logout=="yes") { echo ("<br>You have successfully logged out."); } ?>
        <!-- <a href="">Forgot your password?</a> -->
    </fieldset>
   
</form>

</body>
</html>
