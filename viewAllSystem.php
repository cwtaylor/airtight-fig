<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>ATSSL: Customer Records - All Systems</title>
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
</head>
<body>
<?
//Select table
$query = "SELECT * ".
	"FROM customer_details, customer_systems ".
		"WHERE customer_id = customerid ORDER BY system_id";
$result = mysql_query($query);

function doDate($date)
{
	$phpdate = strtotime($date);
	$newdate = date('F', $phpdate);
	return $newdate;
}


echo "<h1>Customers Systems (All)</h1> <br />";
echo "<table class='zebra'>
<tr>
<th>System ID</th>
<th>Customer ID</th>
<th>System Name</th>
<th>System Type</th>
<th>Contracted?</th>
<th>Contract Month</th>
<th>Maintenance Interval</th>
<th>Monitoring Type</th>
<th>Monitoring Account</th>
<th>URN</th>
<th>Maintenance Cost</th>
<th>Monitoring Cost</th>
<th>Site Address</th>
<th></th>
</tr>";

while($row = mysql_fetch_array($result))
	{
	if (!empty($row['customer_company_name']))
	{
		$systemName = $row['customer_company_name'];
	}else{
		$systemName = $row['customer_title'] . " " . $row['customer_first_name'] . " " . $row['customer_last_name'];
	}
	if ($row['system_maintenance_interval']==="1"){
		$maintInt = "Yearly";
	}elseif ($row['system_maintenance_interval']==="2"){
		$maintInt = "Half-Yearly";
	}elseif ($row['system_maintenance_interval']==="3"){
		$maintInt = "Tri-Yearly";
	}elseif ($row['system_maintenance_interval']==="4"){
		$maintInt = "Quarterly";
	}elseif ($row['system_maintenance_interval']==="12"){
		$maintInt = "Monthly";
	}else{
		$maintInt = "N/A";
	}
	
	$contractDate = doDate($row['system_contract_date']);
	$systemid = $row['system_id'];
	$address = $row['system_address_door_number']. " ". $row['system_address_street_name'] . ", " . $row['system_address_town']. ", " . $row['system_address_county']. ", " . $row['system_address_postcode'];
	  echo "<tr>";
	  echo "<td>" . $row['system_id'] . "</td>";
	  echo "<td>" . $row['customerid'] . "</td>";
	  echo "<td>" . $systemName;
	  echo "<td>" . $row['system_type']. "</td>";
	  echo "<td>" . $row['system_contracted'] . "</td>";
	  echo "<td>" . $contractDate . "</td>";
	  echo "<td>" . $maintInt . "</td>";
	  echo "<td>" . $row['system_monitoring_type'] . "</td>";
	  echo "<td>" . $row['system_monitoring_accnt_num'] . "</td>";
	  echo "<td>" . $row['system_urn'] . "</td>";
	  echo "<td> &pound;" . $row['system_price_ex_vat'] . "</td>";
	  echo "<td> &pound;" . $row['system_monitoring_cost'] . "</td>";
	  echo "<td>" . $address . "</td>";
	  echo "<td><a href=viewSystem.php?cid=" .$row['customer_id'] . "&sid=" . $row['system_id'] . ">view</a> / <a href=updateSystem.php?id=" . $systemid . ">edit</a></td>";
	  echo "</tr>";
	  $maintInt = "";
	}
include("$_SERVER[DOCUMENT_ROOT]/include/footer.php");
?>