<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/overlayDialog/dialog.php");
include("$_SERVER[DOCUMENT_ROOT]/include/phoneFormat.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include("$_SERVER[DOCUMENT_ROOT]/include/postcode/postcode.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>ATSSL: Customer Records - All Jobs</title>
<link rel="stylesheet" type="text/css" href="include/style/table2.css" />
<link rel="stylesheet" href="include/messi/messi.css" />
<script src="include/messi/messi.js"></script>
</head>
<body>
<?
//Select table
$jobNumber = $_GET['jNum'];
if (empty($jobNumber) && !is_numeric($jobNumber)) {
	header("location:index.php?msg=No search term entered."); // Redirect to login.php page
}

/*
$query = "SELECT * ".
	"FROM service_jobs_core, system_contacts ".
		"WHERE service_jobs_core.systemid=system_contacts.systemid AND system_contacts.idsystem_contacts=service_jobs_core.job_contact AND job_id LIKE '$jobNumber%' GROUP BY job_id";
*/
$result = mysql_query("SELECT * FROM service_jobs_core, system_contacts WHERE job_id LIKE '%$jobNumber%' AND service_jobs_core.systemid=system_contacts.systemid AND system_contacts.idsystem_contacts=service_jobs_core.job_contact");
$numResults = mysql_num_rows($result);
if($numResults==0){
	header("location:index.php?msg=No results were found."); // Redirect to login.php page
}

echo "<table class='bordered'>
<tr>
<th>Job ID</th>
<th>System ID</th>
<th>Profile</th>
<th>Status</th>
<th>Owner</th>
<th>Job Contact</th>
<th>Description</th>
<th>Logged On</th>
<th>Logged By</th>
<th>Response Time</th>
<th>Reported By</th>
<th></th>
</tr>";
echo "<h2>Jobs Matching '". $jobNumber . "' (". $numResults . ")</h2>";

while($row = mysql_fetch_array($result))
	{
	$jobid = $row['job_id'];
		$systemid = $row['systemid'];
		$loggedtime = $row['job_reported_time'];
		if($row['job_response_time']<1){
			$response_time = "<font color='green'>" . $row['job_response_time'] * 60 . " minutes</font>";
		}elseif($row['job_response_time']<24){
			$response_time = "<font color='orange'>" . $row['job_response_time'] . " hours</font>";
		}else{
			$response_time = "<font color='red'>" . round($row['job_response_time'] /24,0) . " days</font>";
		}
		$customerid = $row['customerid'];
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/links.php");
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/profile.php");
		
	  echo "<tr>";
	  echo "<td id='load'><a href='viewJobDetail.php?jobID=" . $jobid . "'target='blank'>". $row['job_profile'] . $row['job_id']. "</td>";
	  echo "<td><a href=viewSystem.php?cid=" . $customerid . "&sid=". $row['systemid'] . ">" . $row['systemid'] . "</td>";
	  echo "<td>" . $jobProfile . "</td>";
	  echo "<td>" . $row['job_status'] . "</td>";
	  echo "<td>" . $row['job_owner'] . "</td>";
	  echo "<td>" . $row['system_contact_title'] . " " . $row['system_contact_first_name'] . " " . $row['system_contact_last_name'] . " <i>(" . $row['system_contact_type'] .  ")</i></td>";
	  echo "<td>" . $row['job_description'] . "</td>";
	  echo "<td>" . $row['job_reported_date'] . " - " . $row['job_reported_time'] . "</td>";
	  echo "<td>" . $row['job_logger'] . "</td>";
	  echo "<td>" . $response_time . "</td>";
	  echo "<td>" . $row['job_reporter'] . "</td>";
	  echo "<td>" . $links . "</td>";
	  echo "</tr>"; 
	
	}
include("$_SERVER[DOCUMENT_ROOT]/include/footer.php");
?>
</table>
</body>
</html>
<script src="http://code.jquery.com/jquery-1.6.3.min.js"></script>