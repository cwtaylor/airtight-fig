<html>
<head>
</head>
<body>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<!-- include jQuery FORM Tools (or any other combination) -->
<script src="../../include/jquery/jquery.tools.min.js"></script>
<script src="../../include/date/jquerycustomform.js"></script>

<?php
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/population/dataHelper.php");

$systemid = addslashes($_GET['id']);

echo "<h1>Editing system: " . $systemid . "</h1>";

include("$_SERVER[DOCUMENT_ROOT]/include/footer.php");
$result = mysql_query("SELECT * FROM customer_systems WHERE system_id='$systemid'");
while($row = mysql_fetch_array($result)){
$customerid = $row['customerid'];


echo "<form action=\"doUpdateSystem.php?id=" . $systemid . "\" method=\"post\">";


include("$_SERVER[DOCUMENT_ROOT]/include/population/customerUpdate.php");
?>
<br />

<label>System Type: </label>
<select class="text" name="systemType">
	<? echo "<option>" . $row['system_type'] ."</option>";?>
	<option disabled="disabled">Select a system type</option>
	<? getSystemType() ?>
</select><br />

<label>Contracted: </label>
<select class="text" name="contracted">
	<? echo "<option>" . $row['system_contracted'] ."</option>";?>
	<option disabled="disabled">Select an option</option>
	<option>Yes</option>
	<option>No</option>
</select><br />

<label>Contract Date: </label>
<?php include("$_SERVER[DOCUMENT_ROOT]/include/population/dateUpdateSystem.php");?><br />

<label>Maintenance Interval: </label>
<select class="text" name="system_maintenance_interval">
	<? 
	if ($row['system_maintenance_interval']==="1"){
		$maintInt = "Yearly";
	}elseif ($row['system_maintenance_interval']==="2"){
		$maintInt = "Half-Yearly";
	}elseif ($row['system_maintenance_interval']==="3"){
		$maintInt = "Tri-Yearly";
	}elseif ($row['system_maintenance_interval']==="4"){
		$maintInt = "Quarterly";
	}else{
		$maintInt = "N/A";
	}
	echo "<option value=" . $row['system_maintenance_interval'] . ">" . $maintInt ."</option>";
	
	
	?>
	<option disabled="disabled">Select an option</option>
	<? getMaintenanceInterval() ?>
</select><br />

<label>Maintenance Cost: </label>
<?
echo "<input value=\"". $row['system_price_ex_vat'] . "\"placeholder=\"In GBP\" type=\"text\" name=\"price\" class=\"text\" />";
?>
<br />
<label>Monitoring Cost: </label>
<?
echo "<input value=\"". $row['system_monitoring_cost'] . "\"placeholder=\"In GBP\" type=\"text\" name=\"monitor_price\" class=\"text\" />";
?>
<br />
<label>Monitoring Type: </label>
<select class="text" name="monitorType">
	<? echo "<option>" . $row['system_monitoring_type'] ."</option>";?>
	<option disabled="disabled">Select an option</option>
	<? getMonitoringType() ?>
</select><br />

<label>Monitoring Account Number: </label>
<?
echo "<input value=\"". $row['system_monitoring_accnt_num'] . "\" placeholder=\"Enter Account Number\" type=\"text\" name=\"monitorNum\" class=\"text\" /><br />";
?>
<label>URN: </label>
<?
echo "<input value=\"". $row['system_urn'] . "\" placeholder=\"Enter Account Number\" type=\"text\" name=\"urn\" class=\"text\" /><br />";

echo "<h3>Site Address</h3>";
echo "<label>Name/No: </label>";
echo "<input value=\"" . $row['system_address_door_number'] . "\" type=\"text\" class=\"text\" name=\"houseno\" /><br />";

echo "<label>Street Address: </label>";
echo "<input value=\"" . $row['system_address_street_name'] . "\" type=\"text\" class=\"text\" name=\"streetaddress\" /><br />";

echo "<label>Town: </label>";
echo "<input value=\"" . $row['system_address_town'] . "\" type=\"text\" class=\"text\" name=\"town\" /><br />";

echo "<label>County: </label>";
?>
<?php include("$_SERVER[DOCUMENT_ROOT]/include/population/countiesUpdate_system.php");?><br />

<?
echo "<label>Postcode: </label>";
echo "<input value=\"" . $row['system_address_postcode'] . "\"  type=\"text\" class=\"text\" name=\"postcode\" /><br /><br />";



echo "</div>";

?>

<input type="submit" value="Update" />
<?php

}
?>
</form>

</body>
</html>