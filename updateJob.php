<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/updateJob.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<script type="text/javascript">
function display_div(e) {
    document.getElementById('new1').style.display = "none";
    document.getElementById('passenger2').style.display = "none";
    document.getElementById('passenger3').style.display = "none";
    document.getElementById('new' + e).style.display = "block";
}
</script>
</head>
<body>
<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
include ("$_SERVER[DOCUMENT_ROOT]/adLDAP/src/adLDAP.php");
try {
		$adldap = new adLDAP();
        }
        catch (adLDAPException $e) {
            echo $e; 
            exit();   
        }


$getcid = addslashes($_GET['cid']);
$formattedcid = str_split($getcid,6);
$customerid = $formattedcid[0];

$getsid = addslashes($_GET['sid']);
$formattedsid = str_split($getsid,6);
$systemid = $formattedsid[0];

$getjid = addslashes($_GET['job']);
$formattedjid = str_split($getjid,6);
$jobid = $formattedjid[0];

echo "<h1>Updating Job: " . $jobid . "</h1>";

?>
<form action="include/job/doUpdateJob.php?cid=<?echo $customerid;?>&sid=<?echo $systemid;?>&job=<?echo $jobid;?>" method="post">
<div id="wrapper">
<div id="leftcolumn">
<label>Update: </label><br /><textarea name="description" rows=10 cols=30 ></textarea>
<br />
<label>Travel Time (Hours): </label><br />
<input class ="number" type="number" min="0" max="24" step="0.25" value="0" name="travelTime" />
<br />

<label>Arrival Date/Time: </label><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/arrivalDate.php");?><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/arrivalTime.php");?>
<br />
<label>Departure Date/Time: </label><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/departureDate.php");?><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/departureTime.php");?>
<br />

<input type="submit" value="Submit" />

</div>
<div id="leftcolumn">
<label>Materials Used: </label><br /><textarea name="materials" rows=10 cols=30 ></textarea>
<br />

<label>Update As: </label><br />
<select class="text" name="username">
	<? echo "<option>" . $_SESSION['username'] ."</option>";?>
	<option disabled="disabled">Select a user</option>
	<? $fig_users = $adldap->group()->members('FIG Engineers');
	foreach ($fig_users as $v) {
    $userinfo = $adldap->user()->info($v, array("displayname"));
    $engID = $userinfo[0]["displayname"][0];
    echo "<option value='$v'>$engID</option>";

}?>



	<!-- <option value="alw">Adrian Wilson</option>
	<option value="pwt">Paul Taylor</option>
	<option value="txm">Tim Mullane</option> -->
</select><br />

<label>Action Date/Time: </label><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/actionDate.php");?><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/actionTime.php");?>
</form>
</div>
</div>
</body>
</html><?php

?>