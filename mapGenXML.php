<?php  

include_once("$_SERVER[DOCUMENT_ROOT]/include/connect.php");

// Start XML file, create parent node

$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node); 

// Select all the rows in the markers table

$query = "SELECT * FROM customer_systems";
$result = mysql_query($query);
if (!$result) {  
  die('Invalid query: ' . mysql_error());
} 

header("Content-type: text/xml"); 

// Iterate through the rows, adding XML nodes for each

while ($row = @mysql_fetch_assoc($result)){  
  // ADD TO XML DOCUMENT NODE  
  $node = $dom->createElement("marker");  
  $newnode = $parnode->appendChild($node);   
  $newnode->setAttribute("name",$row['system_id']);
  $newnode->setAttribute("address", $row['system_address_postcode']);  
  $newnode->setAttribute("lat", $row['system_address_lat']);  
  $newnode->setAttribute("lng", $row['system_address_long']);  
  $newnode->setAttribute("type", $row['system_type']);
} 

echo $dom->saveXML();

?>