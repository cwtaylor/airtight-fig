<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/createCustomerLayout.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />
</head>
<body>
<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");


$getcid = addslashes($_GET['cid']);
$formattedcid = str_split($getcid,6);
$customerid = $formattedcid[0];

$getsid = addslashes($_GET['sid']);
$formattedsid = str_split($getsid,6);
$systemid = $formattedsid[0];

$getjid = addslashes($_GET['job']);
$formattedjid = str_split($getjid,6);
$jobid = $formattedjid[0];

echo "<h1>Resolving Job: " . $jobid . "</h1>";

?>
<form action="include/job/doResolveJob.php?cid=<?echo $customerid;?>&sid=<?echo $systemid;?>&job=<?echo $jobid;?>" method="post">
<div id="wrapper">
<div id="leftcolumn">
<label>Update: </label><br /><textarea name="description" rows=10 cols=30 ></textarea>
<br />
<label>Update As: </label><br />
<select class="text" name="username">
	<? echo "<option>" . $_SESSION['username'] ."</option>";?>
	<option disabled="disabled">Select a user</option>
	<option value="alw">Adrian Wilson</option>
	<option value="pwt">Paul Taylor</option>
	<option value="txm">Tim Mullane</option>
</select><br />

<label>Date of Action: </label><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/actionDate.php");?><br />
<br />
<label>Time of Action: </label><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/actionTime.php");?>

<input type="submit" value="Submit" />
</form>
</div>
</div>
</div>
</body>
</html><?php

?>