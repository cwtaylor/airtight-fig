<html>
<head>
</head>
<body>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/createCustomerLayout.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />


<?php

include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
$customerid = addslashes($_GET['id']);
include_once("$_SERVER[DOCUMENT_ROOT]/include/population/addressHelper.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/createSystem/fillCustomer.php");
include("$_SERVER[DOCUMENT_ROOT]/include/population/dataHelper.php");
echo "<h1>Create a System</h1>";

?>

<script type="text/javascript">
function FillBilling(f) {
  if(f.billingtoo.checked == true) {
    f.houseno.value = "<?echo $customer_address_door_number;?>";
	f.streetaddress.value = "<?echo $customer_address_street_name;?>";
	f.town.value = "<?echo $customer_address_town;?>";
	f.county.value = "<?echo $customer_address_county;?>";
	f.country.value = "<?echo $customer_address_country;?>";
	f.postcode.value = "<?echo $customer_address_postcode;?>";
  }
  else{
  f.houseno.value = "";
  f.streetaddress.value = "";
  f.town.value = "";
  f.county.value = "";
  f.country.value = "";
  f.postcode.value = "";
  }
}
</script>
<div id = "wrapper">
<div id = "leftcolumn">
<form action="insertSystem.php" method="post">
<h3>Customer Details</h3>
<?php

if (!empty($companyName))
{
	$customer1 = $companyName;
}else{
	$customer1 = $customer_title . " " . $customer_last_name;
}

?>
<label>Customer: </label>
<input type="text" name="customer1" class="text" value="<? echo $customer1; ?>"><br />
<input type="hidden" name="customer" class="text" value=<?echo $customerid;?> >
<br />
<label>Add customer as keyholder? </label>
<input type="checkbox" name="keyholder">
<br />

<label>System Type: </label>
<select class="text" name="systemType">
<option SELECTED disabled='disabled'>Select a system type</option>
	<? getSystemType() ?>
</select><br />

<label>Contracted: </label>
<select class="text" name="contracted">
	<option SELECTED disabled="disabled">Select an option</option>
	<option>Yes</option>
	<option>No</option>
</select><br />

<label>Contract Date: </label>
<?php include("$_SERVER[DOCUMENT_ROOT]/include/population/date.php");?><br />

<label>Maintenance Interval: </label>
<select class="text" name="system_maintenance_interval">
	<option SELECTED disabled="disabled">Select a maintenance interval</option>
	<? getMaintenanceInterval() ?>
</select><br />

<label>Maintenance Cost: </label>
<input placeholder="In GBP" type="text" name="price" class="text" /><br />

<label>Monitoring Cost: </label>
<input placeholder="In GBP" type="text" name="monitor_price" class="text" /><br />

<label>Monitoring Type: </label>
<select class="text" name="monitorType">
	<option SELECTED disabled="disabled">Select an option</option>
	<? getMonitoringType() ?>
</select><br />

<label>Monitoring Account Number: </label>
<input placeholder="Enter Account Number" type="text" name="monitorNum" class="text" /><br />
<label>URN: </label>
<input placeholder="Enter Account Number" type="text" name="urn" class="text" /><br />
</div>
<div id="rightcolumn">
<h3>Site Address</h3>
<label>Same as invoice address? </label>
<input type="checkbox" name="billingtoo" onclick="FillBilling(this.form)">

<label>Name/No: </label>
<input type="text" class="text" name="houseno" /><br />

<label>Street Address: </label>
<input type="text" class="text" name="streetaddress" /><br />

<label>Town: </label>
<input type="text" class="text" name="town" /><br />

<label>County: </label>
<?php include("$_SERVER[DOCUMENT_ROOT]/include/population/counties.php");?><br />

<label>Country: </label>
<select class="text" name="country">
	<option SELECTED disabled="disabled">Select a country</option>
	<option>England</option>
	<option>Ireland</option>
</select><br />

<label>Postcode: </label>
<input type="text" class="text" name="postcode" /><br /><br />

<input type="submit" />
</div>
<div id = "col3">


</div>
<div id="filler"></div>
</form>
</div>
</body>
</html>