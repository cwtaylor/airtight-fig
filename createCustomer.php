<html>
</head>
<body>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/createCustomerLayout.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />

<?php

include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
?>
<h1>Create a Customer</h1>
<div id = "wrapper">
<div id = "leftcolumn">
<form action="insertCustomer.php" method="post">
<h3>Personal Details</h3>
<label>Title: </label>
<input required="required" type="text" class="text" name="title" /> </br>

<label>Firstname: </label>
<input required="required" type="text" class="text" name="firstname" /> <br />

<label>Lastname: </label>
<input required="required" type="text" class="text" name="lastname" /> <br />

<label>Company Name: </label>
<input type="text" class="text" name="companyname" /> <br />
</div>
<div id = "rightcolumn">
<h3>Contact Details</h3>
<label>Phone: </label>
<input type="tel" class="text" name="phone" /><br />

<label>Mobile: </label>
<input type="tel" class="text" name="mobile" /><br />

<label>Email: </label>
<input type="email" class="text" name="email" /><br />
</div>
<div id="col3">
<h3>Invoice Address</h3>
<label>Name/No: </label>
<input required="required" type="text" class="text" name="invoice_houseno" /><br />

<label>Street Address: </label>
<input required="required" type="text" class="text" name="invoice_streetaddress" /><br />

<label>Town: </label>
<input required="required" type="text" class="text" name="invoice_town" /><br />

<label>County: </label>
<?php include("$_SERVER[DOCUMENT_ROOT]/include/population/invoice_counties.php");?><br />

<label>Country: </label>
<select class="text" name="invoice_country">
	<option SELECTED disabled="disabled">Select a country</option>
	<option>England</option>
	<option>Ireland</option>
</select><br />

<label>Postcode: </label>
<input required="required" type="text" class="text" name="invoice_postcode" /><br />
<input type="submit" />
</div>
<div id="filler"></div>

</form>
</div>

</body>
</html>