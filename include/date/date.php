<!DOCTYPE HTML>
<html>
<body>

<!-- include jQuery FORM Tools (or any other combination) -->
<script src="../../include/jquery/jquery.tools.min.js"></script>
<script src="../../include/population/jquerycustomform.js"></script>

<!-- dateinput styling -->
<link rel="stylesheet" type="text/css" href="../../include/population/dateinput.css"/>

<!-- HTML5 date input -->
<input placeholder="Click to select a date" type="date" />

<!-- make it happen -->
<script>
$(":date").dateinput({dateFormat: 'yyyy-mmmm-dd'});
</script>

</body>
</html>
