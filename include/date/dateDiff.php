<?php
function dateConvert($var)
{
	$date = $var;
	$bits = explode('/',$date);
	$date = $bits[1].'/'.$bits[0].'/'.$bits[2];
	return $date;
}

function dateDiff($date1,$time1,$date2,$time2)
{
	$date1 = dateConvert($date1) . $time1;
	$date2 = dateConvert($date2) . $time2;
	$diff = abs(strtotime($date2) - strtotime($date1))/3600;
	return $diff;
}

?>