<?

require_once("$_SERVER[DOCUMENT_ROOT]/phpmailer/class.phpmailer.php");
define("GUSER", "figbot@atssl.co.uk");
define("GPWD", "I|9#|F%}!r{o3?5");

/* smtpmailer("chris@atssl.co.uk", "000001", "ATF", "assigned"); */

function smtpmailer($to, $job, $profile, $action) { 
	global $error;
	$mail = new PHPMailer();  // create a new object
	$mail->IsSMTP(); // enable SMTP
	$mail->IsHTML(true);
	$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = true;  // authentication enabled
	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 465; 
	$mail->Username = GUSER;  
	$mail->Password = GPWD;    
	
	$message = '<html><body>';
	$message .= '<p>Hello,</p>';
	$message .= "<p>Job " . $profile .  $job . " has been associated to you.</p>";
	$message .= "<p>Please view the job by clicking <a href='https://fig.atssl.co.uk/viewJobDetail.php?jobID=" . $job . "'>here</p>";
	$message .= "<h3>This is an automated message. Replies will not be read.<h3>";
	$message .= '</body></html>';
	
	$subject = "Job " . $profile . $job . " has been " . $action . " to you";
	       
	$mail->SetFrom("figbot@atssl.co.uk", "FigBot");
	$mail->Subject = $subject;
	$mail->Body = $message;
	$mail->AddAddress($to);
	
	if(!$mail->Send()) {
		$error = 'Mail error: '.$mail->ErrorInfo; 
		return false;
	} else {
		$error = 'Message sent!';
		return true;
	}
}

?>