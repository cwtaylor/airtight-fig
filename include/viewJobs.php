<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include//getField.php");
//Select table
$query = "SELECT * ".
	"FROM service_jobs_core, system_contacts ".
		"WHERE service_jobs_core.systemid=system_contacts.systemid AND service_jobs_core.resolutionid=000000 AND service_jobs_core.job_profile='ATF' GROUP BY job_id";
$result = mysql_query($query);
$numResults = mysql_num_rows($result);
echo "<table class='bordered'>
<tr>
<th>Job ID</th>
<th>System ID</th>
<th>Profile</th>
<th>Status</th>
<th>Owner</th>
<th>Job Contact</th>
<th>Description</th>
<th>Logged On</th>
<th>Logged By</th>
<th>Response Time</th>
<th>Reported By</th>
<th></th>
</tr>";
echo "<h2>Faults (" . $numResults . ")</h2>";
while($row = mysql_fetch_array($result))
	{
		$jobid = $row['job_id'];
		$systemid = $row['systemid'];
		$loggedtime = $row['job_reported_time'];
		if(empty($row['job_response_time'])){
			$response_time = "";
		}
		elseif($row['job_response_time']<1){
			$response_time = "<font color='green'>" . $row['job_response_time'] * 60 . " minutes</font>";
		}elseif($row['job_response_time']<24){
			$response_time = "<font color='orange'>" . $row['job_response_time'] . " hours</font>";
		}else{
			$response_time = "<font color='red'>" . round($row['job_response_time'] /24,0) . " days</font>";
		}

		$customerID = $row['customerid'];
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/links.php");
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/profile.php");
		
	  echo "<tr>";
	  echo "<td style='width:6%' id='load'><a href='viewJobDetail.php?jobID=" . $jobid . "'target='blank'>". $row['job_profile'] . $row['job_id']. "</td>";
	  echo "<td style='width:6%'><a href='viewSystem.php?cid=" . $customerID . "&sid=" . $row['systemid'] . "'>" . $row['systemid'] . "</a></td>";
	  echo "<td style='width:6%'>" . $jobProfile . "</td>";
	  echo "<td style='width:6%'>" . $row['job_status'] . "</td>";
	  echo "<td style='width:6%'>" . $row['job_owner'] . "</td>";
	  echo "<td style='width:6%'>" . $row['system_contact_title'] . " " . $row['system_contact_first_name'] . " " . $row['system_contact_last_name'] . " <i>(" . $row['system_contact_type'] .  ")</i></td>";
	  echo "<td style='width:24%'>" . $row['job_description'] . "</td>";
	  echo "<td>" . $row['job_reported_date'] . " - " . $row['job_reported_time'] . "</td>";
	  echo "<td>" . $row['job_logger'] . "</td>";
	  echo "<td>" . $response_time . "</td>";
	  echo "<td>" . $row['job_reporter'] . "</td>";
	  echo "<td style='width:18%'>" . $links . "</td>";
	  echo "</tr>";
	}
	
	//Select table
$query = "SELECT * ".
	"FROM service_jobs_core, system_contacts ".
		"WHERE service_jobs_core.systemid=system_contacts.systemid AND service_jobs_core.resolutionid=000000 AND service_jobs_core.job_profile='ATS' GROUP BY job_id";
$result = mysql_query($query);
$numResults = mysql_num_rows($result);
echo "<table class='bordered'>
<tr>
<th>Job ID</th>
<th>System ID</th>
<th>Profile</th>
<th>Status</th>
<th>Owner</th>
<th>Job Contact</th>
<th>Description</th>
<th>Logged On</th>
<th>Logged By</th>
<th>Response Time</th>
<th>Reported By</th>
<th></th>
</tr>";
echo "<h2>Service Requests (" . $numResults . ")</h2>";
while($row = mysql_fetch_array($result))
	{
		$jobid = $row['job_id'];
		$systemid = $row['systemid'];
		$loggedtime = $row['job_reported_time'];
		if(empty($row['job_response_time'])){
			$response_time = "";
		}
		elseif($row['job_response_time']<1){
			$response_time = "<font color='green'>" . $row['job_response_time'] * 60 . " minutes</font>";
		}elseif($row['job_response_time']<24){
			$response_time = "<font color='orange'>" . $row['job_response_time'] . " hours</font>";
		}else{
			$response_time = "<font color='red'>" . round($row['job_response_time'] /24,0) . " days</font>";
		}

		$customerID = $row['customerid'];
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/links.php");
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/profile.php");
		
	  echo "<tr>";
	  echo "<td style='width:6%' id='load'><a href='viewJobDetail.php?jobID=" . $jobid . "'target='blank'>". $row['job_profile'] . $row['job_id']. "</td>";
	  echo "<td style='width:6%'><a href='viewSystem.php?cid=" . $customerID . "&sid=" . $row['systemid'] . "'>" . $row['systemid'] . "</a></td>";
	  echo "<td style='width:6%'>" . $jobProfile . "</td>";
	  echo "<td style='width:6%'>" . $row['job_status'] . "</td>";
	  echo "<td style='width:6%'>" . $row['job_owner'] . "</td>";
	  echo "<td style='width:6%'>" . $row['system_contact_title'] . " " . $row['system_contact_first_name'] . " " . $row['system_contact_last_name'] . " <i>(" . $row['system_contact_type'] .  ")</i></td>";
	  echo "<td style='width:24%'>" . $row['job_description'] . "</td>";
	  echo "<td>" . $row['job_reported_date'] . " - " . $row['job_reported_time'] . "</td>";
	  echo "<td>" . $row['job_logger'] . "</td>";
	  echo "<td>" . $response_time . "</td>";
	  echo "<td>" . $row['job_reporter'] . "</td>";
	  echo "<td style='width:18%'>" . $links . "</td>";
	  echo "</tr>";
	}
	
	//Select table
$query = "SELECT * ".
	"FROM service_jobs_core, system_contacts ".
			"WHERE service_jobs_core.systemid=system_contacts.systemid AND service_jobs_core.resolutionid=000000 AND service_jobs_core.job_profile='ATM' GROUP BY job_id";
$result = mysql_query($query);
$numResults = mysql_num_rows($result);
echo "<table class='bordered'>
<tr>
<th>Job ID</th>
<th>System ID</th>
<th>Profile</th>
<th>Status</th>
<th>Owner</th>
<th>Job Contact</th>
<th>Phone Number</th>
<th>Description</th>
<th>Logged On</th>
<th>Logged By</th>
<th>Reported By</th>
<th></th>
</tr>";
echo "<h2>Maintenance (" . $numResults . ")</h2>";
while($row = mysql_fetch_array($result))
	{
		$jobid = $row['job_id'];
		$systemid = $row['systemid'];
		$loggedtime = $row['job_reported_time'];


		$customerID = $row['customerid'];
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/links.php");
	  include("$_SERVER[DOCUMENT_ROOT]/include/job/profile.php");
		
	  echo "<tr>";
	  echo "<td style='width:6%' id='load'><a href='viewJobDetail.php?jobID=" . $jobid . "'target='blank'>". $row['job_profile'] . $row['job_id']. "</td>";
	  echo "<td style='width:6%'><a href='viewSystem.php?cid=" . $customerID . "&sid=" . $row['systemid'] . "'>" . $row['systemid'] . "</a></td>";
	  echo "<td style='width:6%'>" . $jobProfile . "</td>";
	  echo "<td style='width:6%'>" . $row['job_status'] . "</td>";
	  echo "<td style='width:6%'>" . $row['job_owner'] . "</td>";
	  echo "<td style='width:6%'>" . $row['system_contact_title'] . " " . $row['system_contact_first_name'] . " " . $row['system_contact_last_name'] . " <i>(" . $row['system_contact_type'] .  ")</i></td>";
	  echo "<td style='width:7%'>" . $row['system_contact_phone'] . "</td>";
	  echo "<td style='width:24%'>" . $row['job_description'] . "</td>";
	  echo "<td>" . $row['job_reported_date'] . " - " . $row['job_reported_time'] . "</td>";
	  echo "<td>" . $row['job_logger'] . "</td>";
	  echo "<td>" . $row['job_reporter'] . "</td>";
	  echo "<td style='width:18%'>" . $links . "</td>";
	  echo "</tr>";
	}
?>
</script>