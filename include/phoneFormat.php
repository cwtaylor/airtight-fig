<?php


function format_telfax2 ($number,$fax=false) {
    // http://james.cridland.net/code/format_uk_phonenumbers.html
    // v2: worked on by Olly Benson to make it look better and work faster!
    // v2.1: removal of a bugette
    // v2.2: fix Cumbria numbers: thank you Roger Miller

    // Change the international number format and remove any non-number character
    $number=ereg_replace( '[^0-9]+','',str_replace("+", "00", $number));

    // Turn number into array based on Telephone Format
    $numberArray = splitNumber($number,explode(",",getTelephoneFormat($number)));

    // Add brackets around first split of numbers if number starts with 01 or 02
    if (substr($number,0,2)=="01" || substr($number,0,2)=="02") $numberArray[0]="(".$numberArray[0].")";

    // Convert array back into string, split by spaces
    $formattedNumber = implode(" ",$numberArray);

	return $formattedNumber;
}

function getTelephoneFormat($number) {
    // This uses full codes from http://www.area-codes.org.uk/formatting.shtml
    $telephoneFormat = array (
        '02' => "3,4,4",
        '03' => "4,3,4",
        '05' => "3,4,4",
        '0500' => "4,6",
        '07' => "5,3,3",
        '070' => "3,4,4",
        '076' => "3,4,4",
        '07624' => "5,6",
        '08' => "4,3,4", // some 0800 numbers are 4,6
        '09' => "4,3,4",
        '01' => "5,3,3", // some 01 numbers are 5,5
        '011' => "4,3,4",
        '0121' => "4,3,4",
        '0131' => "4,3,4",
        '0141' => "4,3,4",
        '0151' => "4,3,4",
        '0161' => "4,3,4",
        '0191' => "4,3,4",
        '013873' => "6,5",
        '015242' => "6,5",
        '015394' => "6,5",
        '015395' => "6,5",
        '015396' => "6,5",
        '016973' => "6,5",
        '016974' => "6,5",
        '016977' => "6,5",
        '0169772' => "6,4",
        '0169773' => "6,4",
        '017683' => "6,5",
        '017684' => "6,5",
        '017687' => "6,5",
        '019467' => "6,5");

    // Sorts into longest key first
    uksort($telephoneFormat, "sortStrLen");

    foreach ($telephoneFormat AS $key=>$value) {
        if (substr($number,0,strlen($key)) == $key) break;
        };
    return $value;
    }

function splitNumber($number,$split) {
    $start=0;
    $array = array();
    foreach($split AS $value) {
        $array[] = substr($number,$start,$value);
        $start = $start+$value;
        }
    return $array;
    }

function sortStrLen($a, $b) {return strlen($b)-strlen($a);}

?>