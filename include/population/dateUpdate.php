<!DOCTYPE HTML>
<html>
<body>

<!-- include jQuery FORM Tools (or any other combination) -->
<script src="http://cdn.jquerytools.org/1.2.6/full/jquery.tools.min.js"></script>
<script src="http://airtightsecuritysystems.co.uk/fig/include/date/jquerycustomform.js"></script>

<!-- dateinput styling -->
<link rel="stylesheet" type="text/css" href="http://airtightsecuritysystems.co.uk/fig/include/date/dateinput.css"/>

<!-- HTML5 date input -->
<?
echo "<input value=\"". $row['system_contract_date'] . "\" placeholder=\"Click to select a date\" name=\"contractDate\" type=\"date\" />";
?>
<!-- make it happen -->
<script>
$(":date").dateinput({dateFormat: 'dd-mmmm-yyyy'});
</script>

</body>
</html>
