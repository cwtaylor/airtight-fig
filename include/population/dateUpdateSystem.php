<!DOCTYPE HTML>
<html>
<body>

<!-- include jQuery FORM Tools (or any other combination) -->
<script src="../../include/jquery/jquery.tools.min.js"></script>
<script src="../../include/date/jquerycustomform.js"></script>

<!-- dateinput styling -->
<link rel="stylesheet" type="text/css" href="../../include/date/dateinput.css"/>

<!-- HTML5 date input -->
<? $store_date = $row['system_contract_date'];
echo "<input placeholder='Click to select a date' name='contractDate' type='date' value=" . $store_date . ">";
?>

<!-- make it happen -->
<script>
$(":date").dateinput({dateFormat: 'dd/mm/yyyy'});
</script>

</body>
</html>
