<?php
set_include_path('/srv/www/fig.atssl.co.uk/public_html/include/');
include_once("connectCron.php");

define("MAPS_HOST", "maps.googleapis.com");
define("KEY", "AIzaSyBX-eIX9FYFbob-F2Lgh8OVdvBldl54zL8");


// Select all the rows in the markers table
$query = "SELECT * FROM customer_systems";
$result = mysql_query($query);
if (!$result) {
  die("Invalid query: " . mysql_error());
}

// Initialize delay in geocode speed
$delay = 0;
$base_url = "http://" . MAPS_HOST . "/maps/api/geocode/xml?" . "sensor=false";

// Iterate through the rows, geocoding each address
while ($row = @mysql_fetch_assoc($result)) {
  $geocode_pending = true;

  while ($geocode_pending) {
    $address = $row["system_address_postcode"];
    $id = $row["system_id"];
    $request_url = $base_url . "&address=" . urlencode($address);
/*     echo $request_url; */
    $xml = simplexml_load_file($request_url) or die("url not loading");

    $status = $xml->status;
/*     echo "Status is: " . $status; */
    if (strcmp($status, "OK") == 0) {
      // Successful geocode
      $geocode_pending = false;
/*
      $coordinatesLat = $xml->result->geometry->location->lat;
      echo $coordinates;
      $coordinatesSplit = split(",", $coordinates);
*/
      // Format: Longitude, Latitude, Altitude
      $lat = $xml->result->geometry->location->lat;
      $lng = $xml->result->geometry->location->lng;

      $query = sprintf("UPDATE customer_systems " .
             " SET system_address_lat = '%s', system_address_long = '%s' " .
             " WHERE system_id = '%s' LIMIT 1;",
             mysql_real_escape_string($lat),
             mysql_real_escape_string($lng),
             mysql_real_escape_string($id));
      $update_result = mysql_query($query);
      if (!$update_result) {
        die("Invalid query: " . mysql_error());
      }
    } else if (strcmp($status, "OVER_QUERY_LIMIT") == 0) {
      // sent geocodes too fast
      $delay += 100000;
    } else {
      // failure to geocode
      $geocode_pending = false;
      echo "Address " . $address . " failed to geocoded. ";
      echo "Received status " . $status . "
\n";
    }
    usleep($delay);
  }
}
?>
