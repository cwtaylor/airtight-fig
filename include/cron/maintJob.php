<?php
/* doMaint("000001", "000021", "Mr Malone", "01/11/2012", "2"); */

function doDate($date, $diff)
{
	$bits = explode('/',$date);
	$date = $bits[1].'/'.$bits[0].'/'.$bits[2];
	$newdate = strtotime ( $diff , strtotime ( $date ) ) ;
	$newdate = date ( 'd/m/Y' , $newdate );
	return $newdate;
}

function testMaint($date)
{
	$thisMonth = date("m",strtotime("+1 months"));
	if(substr($date,3,2) == $thisMonth)
	{
		$val = $date . " is true.";
		return "true";
	}else{
		$val = $date . " is false.";
		return "false";
	}
}	

function doMaint($customerid, $systemid, $jobcontact, $date1, $interval)
{
	$thisMonth = date("d/m/Y");
	if($interval==="1"){
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("1", "1", $customerid, $systemid, $jobcontact, $date2);
		}
	}elseif($interval==="2"){
		$date2 = doDate($date1, "+6 months");
		if(testMaint($date2)==="true"){
			logJob("1", "2", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("2", "2", $customerid, $systemid, $jobcontact, $date2);
		}
	}elseif($interval==="3"){
		$date2 = doDate($date1, "+4 months");
		if(testMaint($date2)==="true"){
			logJob("1", "3", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+8 months");
		if(testMaint($date2)==="true"){
			logJob("2", "3", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("3", "3", $customerid, $systemid, $jobcontact, $date2);
		}
	}elseif($interval==="4"){
		$date2 = doDate($date1, "+3 months");
		if(testMaint($date2)==="true"){
			logJob("1", "4", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+6 months");
		if(testMaint($date2)==="true"){
			logJob("2", "4", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+9 months");
		if(testMaint($date2)==="true"){
			logJob("3", "4", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("4", "4", $customerid, $systemid, $jobcontact, $date2);	
		}
	}elseif($interval==="12"){
		$date2 = doDate($date1, "+1 months");
		if(testMaint($date2)==="true"){
			logJob("1", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+2 months");
		if(testMaint($date2)==="true"){
			logJob("2", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+3 months");
		if(testMaint($date2)==="true"){
			logJob("3", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+4 months");
		if(testMaint($date2)==="true"){
			logJob("4", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+5 months");
		if(testMaint($date2)==="true"){
			logJob("5", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+6 months");
		if(testMaint($date2)==="true"){
			logJob("6", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+7 months");
		if(testMaint($date2)==="true"){
			logJob("7", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+8 months");
		if(testMaint($date2)==="true"){
			logJob("8", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+9 months");
		if(testMaint($date2)==="true"){
			logJob("9", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+10 months");
		if(testMaint($date2)==="true"){
			logJob("10", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+11 months");
		if(testMaint($date2)==="true"){
			logJob("11", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+12 months");
		if(testMaint($date2)==="true"){
			logJob("12", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
	}else{
		echo "Invalid input";
	}
}

function logJob($maint, $maintTotal, $customerid, $systemid, $jobcontact, $dueDate)
{
	//Declare variables
$host_name = 'localhost';
$user_name = 'fig_user';
$pass_word = 'Enterpr15e';
$database_name = 'fig';

//Connect to database
$conn = mysql_connect($host_name, $user_name, $pass_word) or die ('Error connecting to mysql');

//Select schema
mysql_select_db($database_name);

	$jobstatus = "Unassigned";
	$jobowner = "";
	$jobdescription = "Maintenance Request due by " . $dueDate . ". This is maintenance " .$maint . "/" . $maintTotal;
	$jobreporteddate = date("d/m/Y");
	$jobreportedtime = date("G:i");
	$jobreporter = "FigBot";
	$joblogger = "FigBot";
	$jobProfile = "ATM";
	
	
	echo "Logging Job for customer ID: " . $customerid . "<br />";
	echo "System ID: " . $systemid . "<br />";
	echo "Job Status: " . $jobstatus . "<br />";
	echo "Job Owner: " . $jobowner . "<br />";
	echo "Job Contact: " . $jobcontact . "<br />";
	echo "Job Description: " . $jobdescription . "<br />";
	echo "Job Reported Date: " . $jobreporteddate . "<br />";
	echo "Job Reported Time: " . $jobreportedtime . "<br />";
	echo "Job Reporter: " . $jobreporter . "<br />";
	echo "Job Logger: " . $joblogger . "<br />";
	echo "Job Profile: " . $jobProfile . "<br /><br />";
	
	$sql="INSERT INTO service_jobs_core (customerid, systemid, job_status, job_owner, job_contact, job_description, job_reported_date, job_reported_time, job_reporter, job_logger, job_profile) VALUES('$customerid','$systemid','$jobstatus','$jobowner','$jobcontact','$jobdescription','$jobreporteddate','$jobreportedtime','$jobreporter','$joblogger','$jobProfile')";
		
		if (!mysql_query($sql,$conn))
  		{
 		 die('Error: ' . mysql_error());
 		 }
 		 
$jobID = mysql_insert_id();
$detailOwner = $jobreporter;
$detailComments = "New Maintenance Request Submitted";
$detailDate = date("d/m/Y");
$detailTime = date("G:i");
 		 
 		 $sql="INSERT INTO service_jobs_details (jobid, detail_owner, detail_comments, detail_date, detail_time)
VALUES('$jobID','$detailOwner','$detailComments','$detailDate','$detailTime')";
		
		if (!mysql_query($sql,$conn))
  		{
 		 die('Error: ' . mysql_error());
 		 }
 		 $detailComments = $jobdescription;
 		 $sql="INSERT INTO service_jobs_details (jobid, detail_owner, detail_comments, detail_date, detail_time)
VALUES('$jobID','$detailOwner','$detailComments','$detailDate','$detailTime')";
		
		if (!mysql_query($sql,$conn))
  		{
 		 die('Error: ' . mysql_error());
 		 }
}


?>

