<?php
/* doMaint("000001", "000021", "Mr Malone", "01/11/2012", "2"); */
include_once("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/cron/getField2.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
?>
<link rel='stylesheet' type='text/css' href='../../include/style/table2.css' />
<?
$total = 0;
setupTable();
connector();
$thisMonth = addslashes($_GET['m']);
	$shortMonth =  date("F",mktime(0,0,0,$thisMonth,1,0));
echo "<h1>Maintainance in " . $shortMonth . " (" . $total . ")</h1> <br />";

function doDate($date, $diff)
{
	$bits = explode('/',$date);
	$date = $bits[1].'/'.$bits[0].'/'.$bits[2];
	$newdate = strtotime ( $diff , strtotime ( $date ) ) ;
	$newdate = date ( 'd/m/Y' , $newdate );
	return $newdate;
}

function testMaint($date)
{
/* 	$thisMonth = date("m",strtotime("+1 months")); */
	$thisMonth = addslashes($_GET['m']);
	if(substr($date,3,2) == $thisMonth)
	{
		$val = $date . " is true.";
		return "true";
	}else{
		$val = $date . " is false.";
		return "false";
	}
}	

function doMaint($customerid, $systemid, $jobcontact, $date1, $interval)
{
	$thisMonth = date("d/m/Y");
	if($interval==="1"){
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("1", "1", $customerid, $systemid, $jobcontact, $date2);
		}
	}elseif($interval==="2"){
		$date2 = doDate($date1, "+6 months");
		if(testMaint($date2)==="true"){
			logJob("1", "2", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("2", "2", $customerid, $systemid, $jobcontact, $date2);
		}
	}elseif($interval==="3"){
		$date2 = doDate($date1, "+4 months");
		if(testMaint($date2)==="true"){
			logJob("1", "3", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+8 months");
		if(testMaint($date2)==="true"){
			logJob("2", "3", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("3", "3", $customerid, $systemid, $jobcontact, $date2);
		}
	}elseif($interval==="4"){
		$date2 = doDate($date1, "+3 months");
		if(testMaint($date2)==="true"){
			logJob("1", "4", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+6 months");
		if(testMaint($date2)==="true"){
			logJob("2", "4", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+9 months");
		if(testMaint($date2)==="true"){
			logJob("3", "4", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+1 year");
		if(testMaint($date2)==="true"){
			logJob("4", "4", $customerid, $systemid, $jobcontact, $date2);	
		}
	}elseif($interval==="12"){
		$date2 = doDate($date1, "+1 months");
		if(testMaint($date2)==="true"){
			logJob("1", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+2 months");
		if(testMaint($date2)==="true"){
			logJob("2", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+3 months");
		if(testMaint($date2)==="true"){
			logJob("3", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+4 months");
		if(testMaint($date2)==="true"){
			logJob("4", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+5 months");
		if(testMaint($date2)==="true"){
			logJob("5", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+6 months");
		if(testMaint($date2)==="true"){
			logJob("6", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+7 months");
		if(testMaint($date2)==="true"){
			logJob("7", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+8 months");
		if(testMaint($date2)==="true"){
			logJob("8", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+9 months");
		if(testMaint($date2)==="true"){
			logJob("9", "12", $customerid, $systemid, $jobcontact, $date2);
		}
		$date2 = doDate($date1, "+10 months");
		if(testMaint($date2)==="true"){
			logJob("10", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+11 months");
		if(testMaint($date2)==="true"){
			logJob("11", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
		$date2 = doDate($date1, "+12 months");
		if(testMaint($date2)==="true"){
			logJob("12", "12", $customerid, $systemid, $jobcontact, $date2);	
		}
	}else{
		echo "Invalid input";
	}
}

function connector()
{
	//Select table
$query = "SELECT * ".
	"FROM customer_details, customer_systems ".
		"WHERE customer_id = customerid AND system_contract_date != ''";
$result = mysql_query($query);

while($row = mysql_fetch_array($result))
	{
		$customerid = $row['customerid'];
		$systemid = $row['system_id'];
		$contractDate = $row['system_contract_date'];
		$maintInterval = $row['system_maintenance_interval'];
		$contactID = getField2("system_contacts", "systemid", "$systemid", "system_contact_type", "'Owner'", "idsystem_contacts");
		//echo $contractDate;
		doMaint($customerid, $systemid, $contactID, "$contractDate", $maintInterval);
	}


}

function setupTable()
{
	global $total;
	echo "<link rel='stylesheet' type='text/css' href='include/style/table2.css' />";
	/* echo "<h1>Maintainance in " . $shortMonth . " (" . $total . ")</h1> <br />"; */
	echo "<table class='zebra'>
	<tr>
	<th>System ID</th>
	<th>Customer ID</th>
	<th>System Name</th>
	<th>System Type</th>
	<th>Contracted?</th>
	<th>Contract Month</th>
	<th>Maintenance Interval</th>
	<th>Monitoring Type</th>
	<th>Monitoring Account Number</th>
	<th>Price Ex VAT</th>
	<th>Site Address</th>
	<th></th>
	</tr>";
}

function logJob($maint, $maintTotal, $customerid, $systemid, $jobcontact, $dueDate)
{
	global $total;
	$total = $total + 1;
	$jobstatus = "Unassigned";
	$jobowner = "";
	$jobdescription = "Maintenance Request due by " . $dueDate . ". This is maintenance " .$maint . "/" . $maintTotal;
	$jobreporteddate = date("d/m/Y");
	$jobreportedtime = date("G:i");
	$jobreporter = "FigBot";
	$joblogger = "FigBot";
	$jobProfile = "ATM";
	
	echo "<tr>";
	echo "<td>" . $systemid . "</td>";
	echo "<td>" . $customerid . "</td>";
	echo "<td>" . "System Name" . "</td>";
	echo "<td>" . "System Type" . "</td>";
	echo "<td>" . "Contracted?" . "</td>";
	echo "<td>" . $dueDate . "</td>";
	echo "<td>" . $maintTotal . "</td>";
	echo "<td>" . "Monitoring Type" . "</td>";
	echo "<td>" . "Monitoring Account Number" . "</td>";
	echo "<td>" . "Price Ex VAT" . "</td>";
	echo "<td>" . "Site Address" . "</td>";
	echo "</tr>";
	
/*
	echo "Logging Job for customer ID: " . $customerid . "<br />";
	echo "System ID: " . $systemid . "<br />";
	echo "Job Status: " . $jobstatus . "<br />";
	echo "Job Owner: " . $jobowner . "<br />";
	echo "Job Contact: " . $jobcontact . "<br />";
	echo "Job Description: " . $jobdescription . "<br />";
	echo "Job Reported Date: " . $jobreporteddate . "<br />";
	echo "Job Reported Time: " . $jobreportedtime . "<br />";
	echo "Job Reporter: " . $jobreporter . "<br />";
	echo "Job Logger: " . $joblogger . "<br />";
	echo "Job Profile: " . $jobProfile . "<br /><br />";
*/
	
	 		 

}


?>

