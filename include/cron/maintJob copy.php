<?php
function doDate($date, $diff)
{
	$bits = explode('/',$date);
	$date = $bits[1].'/'.$bits[0].'/'.$bits[2];
	$newdate = strtotime ( $diff , strtotime ( $date ) ) ;
	$newdate = date ( 'd/m/Y' , $newdate );
	return $newdate;
}

function testMaint($date)
{
	$thisMonth = date("m",strtotime("+1 months"));
	if(substr($date,3,2) == $thisMonth)
	{
		$val = $date . " is true.";
		return "true";
	}else{
		$val = $date . " is false.";
		return "false";
	}

}

function doMaint($date1, $interval)
{
	$thisMonth = date("d/m/Y");
	if($interval==="1"){
		echo "Interval is: Once a year";
		echo "<br />";
		echo "Original date is: " . $date1;
		echo "<br />";
		echo "1st Maint is: " . testMaint(doDate($date1, "+1 year"));
	}elseif($interval==="2"){
		echo "Twice a year \n";
		echo "<br />";
		echo "Original date is: " . $date1;
		echo "<br />";
		echo "1st Maint is: " . $date1, "+6 months";
		echo "<br />";
		echo "2nd Maint is: " . doDate($date1, "+1 year");
		echo "<br />";
	}elseif($interval==="3"){
		echo "Thrice a year \n";
		echo "<br />";
		echo "Original date is: " . $date1;
		echo "<br />";
		echo "1st Maint is: " . doDate($date1, "+4 months");
		echo "<br />";
		echo "2nd Maint is: " . doDate($date1, "+8 months");
		echo "<br />";
		echo "3rd Maint is: " . doDate($date1, "+1 year");
		echo "<br />";
	}elseif($interval==="4"){
		echo "Four times a year";
		echo "<br />";
		echo "Original date is: " . $date1;
		echo "<br />";
		echo "1st Maint is: " . doDate($date1, "+3 months");
		echo "<br />";
		echo "2nd Maint is: " . doDate($date1, "+6 months");
		echo "<br />";
		echo "3rd Maint is: " . doDate($date1, "+9 months");
		echo "<br />";
		echo "4th Maint is: " . doDate($date1, "+1 year");
		echo "<br />";
	}else{
		echo "Invalid input";
	}

}

function logJob($customerid, $systemid, $jobcontact, $dueDate)
{
	$jobstatus = "New Maintenances";
	$jobowner = "";
	$jobdescription = "Maintenance Request due by " . $dueDate;
	$jobreporteddate = date("d/m/Y");
	$jobreportedtime = date("G:i");
	$jobreporter = "FigBot";
	$joblogger = "FigBot";
	$jobProfile = "ATM";
	
	echo "Logging Job for customer ID: " . $customerid . "<br />";
	echo "System ID: " . $systemid . "<br />";
	echo "Job Status: " . $jobstatus . "<br />";
	echo "Job Owner: " . $jobowner . "<br />";
	echo "Job Contact: " . $jobcontact . "<br />";
	echo "Job Description: " . $jobdescription . "<br />";
	echo "Job Reported Date: " . $jobreporteddate . "<br />";
	echo "Job Reported Time: " . $jobreportedtime . "<br />";
	echo "Job Reporter: " . $jobreporter . "<br />";
	echo "Job Logger: " . $joblogger . "<br />";
	echo "Job Profile: " . $jobProfile . "<br />";
}

/* logJob("000001", "000023", "Mr Malone", "02/03/2013"); */
doMaint("01/02/2013", "4");	
?>