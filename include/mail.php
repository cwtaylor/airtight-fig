<?php
function sendEmail($to, $job, $profile, $action)
{
	$subject = "FIG: Job '" . $profile . $job . "'";
	
	$message = '<html><body>';
	$message .= '<p>Hello,</p>';
	$message .= "<p>Job " . $profile .  $job . " has been associated to you.</p>";
	$message .= "<p>Please view the job by clicking <a href='https://fig.atssl.co.uk/viewJobDetail.php?jobID=" . $job . "'>here</p>";
	$message .= "<h3>This is an automated message. Replies will not be read.<h3>";
	$message .= '</body></html>';
	$from = "FigBot <figbot@atssl.co.uk>";
	$headers = "From:" . $from . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	mail($to, $subject, $message, $headers);
	echo "Mail Sent";
}

/* sendEmail("chris@atssl.co.uk", "00001", "Associated") */
?>
