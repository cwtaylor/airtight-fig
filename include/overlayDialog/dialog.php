<script src="../../include/jquery/jquery.tools.min.js"></script>
<style>
.modal {
	background-color:#fff;
	display:none;
	width:350px;
	padding:15px;
	text-align:left;
	border:2px solid #333;		
	
	opacity:0.8;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	-moz-box-shadow: 0 0 50px #ccc;
	-webkit-box-shadow: 0 0 50px #ccc;
	}
	
.modal h2 {
	background:url(info.png) 0 50% no-repeat;
	margin:0px;
	padding:10px 0 10px 45px;
	border-bottom:1px solid #333;
	font-size:20px;
	}
</style>
<p>

</p>

<div class="modal" id="yesno">
	<h2>Customer Delete Confirmation</h2>

	<p>
		Are you sure you want to delete this customer?
	</p>

	<!-- yes/no buttons -->
	<p>
		<form action="deleteCustomer.php?" method="post">
			<button type="submit" formaction="">No</button>
			<button type="submit">Yes</button>
		</form>
	</p>
</div>


<script>
// What is $(document).ready ? See: http://flowplayer.org/tools/documentation/basics.html#document_ready
$(document).ready(function() {

var triggers = $(".modalInput").overlay({

	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#ebecff',
		loadSpeed: 200,
		opacity: 0.9
	},

	closeOnClick: true
});


var buttons = $("#yesno button").click(function(e) {
	
	// get user input
	var yes = buttons.index(this) === 0;

	// do something with the answer
	triggers.eq(0).html("You clicked " + (yes ? "yes" : "no"));
});


$("#prompt form").submit(function(e) {

	// close the overlay
	triggers.eq(1).overlay().close();

	// get user input
	var input = $("input", this).val();

	// do something with the answer
	triggers.eq(1).html(input);

	// do not submit the form
	return e.preventDefault();
});

});
</script>