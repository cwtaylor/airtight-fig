<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="include/date/dateinput.css"/>
<link rel="stylesheet" type="text/css" href="include/style/createCustomerLayout.css"/>
<link rel="stylesheet" type="text/css" href="include/style/main.css" />
<script type="text/javascript">
function display_div(e) {
    document.getElementById('new1').style.display = "none";
    document.getElementById('passenger2').style.display = "none";
    document.getElementById('passenger3').style.display = "none";
    document.getElementById('new' + e).style.display = "block";
}
</script>
</head>
<body>
<?php
include("$_SERVER[DOCUMENT_ROOT]/include/connect.php");
include("$_SERVER[DOCUMENT_ROOT]/include/checkLogin.php");
include_once("$_SERVER[DOCUMENT_ROOT]/include/header.php");
echo "<h1>Create a Job - Part 2</h1>";

$getcid = addslashes($_GET['cid']);
$formattedcid = str_split($getcid,6);
$customerid = $formattedcid[0];

if ($_GET['sid'] == ""){
	$getsid = $_POST['systemid'];
}else{
	$getsid = addslashes($_GET['sid']);
}

$formattedsid = str_split($getsid,6);
$systemid = $formattedsid[0];

?>
<form action="insertJob.php?cid=<?echo $customerid;?>&sid=<?echo $systemid;?>" method="post">
<div id="wrapper">
<div id="leftcolumn">
<?php
include("$_SERVER[DOCUMENT_ROOT]/include/newJob/customer.php");
//echo "<br />";
include("$_SERVER[DOCUMENT_ROOT]/include/newJob/systemFilled.php");
echo "<br />";
include("$_SERVER[DOCUMENT_ROOT]/include/newJob/jobProfile.php");
include("$_SERVER[DOCUMENT_ROOT]/include/newJob/contacts.php");
echo "<br />";
include("$_SERVER[DOCUMENT_ROOT]/include/newJob/description.php");
?><br />
<label>Date of Action: </label><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/actionDate.php");?><br />
<br />
<label>Time of Action: </label><br />
<?php include("$_SERVER[DOCUMENT_ROOT]/include/date/actionTime.php");?>
<br />


<input type="submit" value="Submit" />
</form>
</div>
</div>
<? include_once("$_SERVER[DOCUMENT_ROOT]/include/newJob/keyholderInput.php"); ?>
</div>
</body>
</html>